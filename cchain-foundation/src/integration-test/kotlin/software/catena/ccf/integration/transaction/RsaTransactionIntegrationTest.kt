package software.catena.ccf.integration.transaction

import software.catena.ccf.CCFService
import software.catena.ccf.crypto.AbstractKeyPair
import software.catena.ccf.crypto.KeyPairFactory
import software.catena.ccf.infrastructure.OkHttpClientFactory
import software.catena.ccf.integration.TestProperties
import software.catena.ccf.json.CCFJsonSerializer
import software.catena.ccf.models.transaction.SingleTransactionResponse
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.*
import java.time.OffsetDateTime
import java.util.*

@Tag("integrationTest")
class RsaTransactionIntegrationTest {

    // setup keys for the creator of the chain
    private val alice: AbstractKeyPair = KeyPairFactory.generateKeyPairRsa()
    private val bob: AbstractKeyPair = KeyPairFactory.generateKeyPairRsa()
    private val evil: AbstractKeyPair = KeyPairFactory.generateKeyPairRsa()

    // CCFService that is used for interacting with the CCM
    private  lateinit  var ccfAlice: CCFService
    private  lateinit  var ccfBob: CCFService
    private  lateinit  var ccfEvil: CCFService

    private lateinit var chainIdAlice: UUID

    /*
     * Before each test is executed, the CCFService will be initialized and
     * a chain will be created for further tests.
     */
    @BeforeEach
    @Throws(Exception::class)
    fun setUp() {
        // Create ccf service, alice is always the sender
        ccfAlice = CCFService(
                keyPair = alice,
                httpUrl = TestProperties.getCCMConnectionRsa(),
                clientType = OkHttpClientFactory.ClientType.HTTPS_CA_TRUSTED_BY_SYSTEM,
                security = CCFService.SecurityMode.VERY_FAST
        )

        ccfBob = CCFService(
                keyPair = bob,
                httpUrl = TestProperties.getCCMConnectionRsa(),
                clientType = OkHttpClientFactory.ClientType.HTTPS_CA_TRUSTED_BY_SYSTEM,
                security = CCFService.SecurityMode.VERY_FAST
        )

        ccfEvil = CCFService(
                keyPair = evil,
                httpUrl = TestProperties.getCCMConnectionRsa(),
                clientType = OkHttpClientFactory.ClientType.HTTPS_CA_TRUSTED_BY_SYSTEM,
                security = CCFService.SecurityMode.VERY_FAST
        )

        // Create a new chain for Alice
        chainIdAlice = ccfAlice.createChain(
                name = "Chain of Alice at " + OffsetDateTime.now().toString(),
                description = "This is a chain of Alice",
                closed = false,
                publicRead = true,
                publicWrite = true
        ).get().chain.chainId
    }

    @Test
    @DisplayName("Create Transaction Plaintext")
    fun createTransactionPlain(){
        // Create a plain transaction
        val plainMessage: String = "Hello Me: " + OffsetDateTime.now().toString()
        val singleTransactionResponseOptional: Optional<SingleTransactionResponse> = ccfAlice.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = alice.cryptId,
                payload = plainMessage.encodeToByteArray(),
                plain = true)
        Assertions.assertThat(singleTransactionResponseOptional.isPresent)
        val transactionResponse: SingleTransactionResponse = singleTransactionResponseOptional.get()

        // Check that the transaction properties are like supposed
        Assertions.assertThat(transactionResponse.transaction).isNotNull
        Assertions.assertThat(String(transactionResponse.payload)).isEqualTo(plainMessage)
        // TODO PW: After introducing PublicKey format for publicKeySender and publicKeyRecipient, also add two new assertions for that
        Assertions.assertThat(transactionResponse.transaction.publicKeySender).isEqualTo(alice.publicKey.encoded)
        Assertions.assertThat(transactionResponse.transaction.publicKeyRecipient).isEqualTo(alice.publicKey.encoded)
        Assertions.assertThat(transactionResponse.transaction.chainId).isEqualTo(chainIdAlice)
        Assertions.assertThat(transactionResponse.transaction.symmetricKeyRecipient).isNull()
        Assertions.assertThat(transactionResponse.transaction.symmetricKeySender).isNull()

        // Check that the chainInfo of the transaction in connected with the correct chain
        Assertions.assertThat(transactionResponse.chainInfo.chainId).isEqualTo(chainIdAlice)
    }


    @Test
    @DisplayName("Create Transaction Encrypted")
    fun createTransactionEncrypted(){
        // Create a plain transaction
        val plainMessage: String = "Hello Me: " + OffsetDateTime.now().toString()
        val singleTransactionResponseOptional: Optional<SingleTransactionResponse> = ccfAlice.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = alice.cryptId,
                payload = plainMessage.toByteArray(),
                plain = false)
        Assertions.assertThat(singleTransactionResponseOptional.isPresent)
        val transactionResponse: SingleTransactionResponse = singleTransactionResponseOptional.get()

        // Check that the transaction properties are like supposed
        Assertions.assertThat(String(transactionResponse.payload)).isEqualTo(plainMessage)
        // TODO PW: After introducing PublicKey format for publicKeySender and publicKeyRecipient, also add two new assertions for that
        Assertions.assertThat(transactionResponse.transaction.publicKeySender).isEqualTo(alice.publicKey.encoded)
        Assertions.assertThat(transactionResponse.transaction.publicKeyRecipient).isEqualTo(alice.publicKey.encoded)
        Assertions.assertThat(transactionResponse.transaction.chainId).isEqualTo(chainIdAlice)
        Assertions.assertThat(transactionResponse.transaction.symmetricKeyRecipient).isNotNull
        Assertions.assertThat(transactionResponse.transaction.symmetricKeySender).isNotNull

        // Check that the chainInfo of the transaction in connected with the correct chain
        Assertions.assertThat(transactionResponse.chainInfo.chainId).isEqualTo(chainIdAlice)
    }

    @Test
    @DisplayName("Get Transaction by id")
    fun getTransactionById(){
        // Send a plain message from alice to bob
        val plainMessage: String = "Hello Bob: " + OffsetDateTime.now().toString()
        val singleTransactionResponseOptional: Optional<SingleTransactionResponse> = ccfAlice.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = bob.cryptId,
                payload = plainMessage.encodeToByteArray(),
                plain = true)
        Assertions.assertThat(singleTransactionResponseOptional.isPresent)
        val transactionResponse: SingleTransactionResponse = singleTransactionResponseOptional.get()

        // Bob reads the message sent by Alice
        val singleTransactionResponseBobOptional: Optional<SingleTransactionResponse> = ccfBob.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(singleTransactionResponseBobOptional.isPresent)
        val transactionResponseBob = singleTransactionResponseBobOptional.get()
        // check payload
        Assertions.assertThat(String(transactionResponse.payload)).isEqualTo(plainMessage)
        // check transaction
        Assertions.assertThat(transactionResponseBob.transaction.publicKeySender).isEqualTo(alice.publicKey.encoded)
        Assertions.assertThat(transactionResponseBob.transaction.publicKeyRecipient).isEqualTo(bob.publicKey.encoded)
        // check chainInfo
        Assertions.assertThat(transactionResponseBob.chainInfo.chainId).isEqualTo(chainIdAlice)

    }

    @Test
    @DisplayName("Send Transaction Plaintext")
    fun sendTransactionPlainString(){
        // Send a plain message from alice to bob
        val plainMessage: String = "Hello Bob: " + OffsetDateTime.now().toString()
        val singleTransactionResponseOptional: Optional<SingleTransactionResponse> = ccfAlice.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = bob.cryptId,
                payload = plainMessage.encodeToByteArray(),
                plain = true)

        Assertions.assertThat(singleTransactionResponseOptional.isPresent)
        val transactionResponse: SingleTransactionResponse = singleTransactionResponseOptional.get()

        // Bob reads the message sent by Alice
        val transactionResponseBob = ccfBob.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(String(transactionResponseBob.get().payload)).isEqualTo(plainMessage)

        // Evil reads the message form Alice as well
        val transactionResponseEvil = ccfEvil.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(String(transactionResponseEvil.get().payload)).isEqualTo(plainMessage)

    }

    @Test
    @DisplayName("Send Transaction Encrypted")
    fun sendTransactionEncryptedString(){
        // Send a plain message from alice to bob
        val plainMessage: String = "Hello Bob: " + OffsetDateTime.now().toString()
        val singleTransactionResponseOptional: Optional<SingleTransactionResponse> = ccfAlice.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = bob.cryptId,
                payload = plainMessage.encodeToByteArray(),
                plain = false)

        Assertions.assertThat(singleTransactionResponseOptional.isPresent)
        val transactionResponse: SingleTransactionResponse = singleTransactionResponseOptional.get()

        // Bob reads the message sent by Alice
        val transactionResponseBob = ccfBob.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(String(transactionResponseBob.get().payload)).isEqualTo(plainMessage)

        // Evil reads the message form Alice as well
        val transactionResponseEvil = ccfEvil.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(transactionResponseEvil.isEmpty)

    }

    @Test
    @DisplayName("Send Transaction Plain Object")
    fun sendTransactionPlainObject(){
        // Send a plain message from alice to bob
        val plainObject: TestPayloadUser = TestPayloadUser(
                name = "Hello Bob: " + OffsetDateTime.now().toString(),
                age = 20,
                city = "Munich")

        val singleTransactionResponseOptional: Optional<SingleTransactionResponse> = ccfAlice.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = bob.cryptId,
                payload = CCFJsonSerializer.toJsonBytesUtf8(plainObject),
                plain = true)
        Assertions.assertThat(singleTransactionResponseOptional.isPresent)
        val transactionResponse: SingleTransactionResponse = singleTransactionResponseOptional.get()

        // Bob reads the message sent by Alice
        val transactionResponseBob = ccfBob.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(CCFJsonSerializer.fromJson(TestPayloadUser::class.java, transactionResponseBob.get().payload)).isEqualTo(plainObject)

        // Evil reads the message form Alice as well
        val transactionResponseEvil = ccfEvil.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(CCFJsonSerializer.fromJson(TestPayloadUser::class.java, transactionResponseEvil.get().payload)).isEqualTo(plainObject)
    }

    @Test
    @DisplayName("Send Transaction Encrypted Object")
    fun sendTransactionEncryptedObject(){
        // Send a plain message from alice to bob
        val plainObject: TestPayloadUser = TestPayloadUser(
                name = "Hello Bob: " + OffsetDateTime.now().toString(),
                age = 20,
                city = "Munich")

        val singleTransactionResponseOptional: Optional<SingleTransactionResponse> = ccfAlice.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = bob.cryptId,
                payload = CCFJsonSerializer.toJsonBytesUtf8(plainObject),
                plain = false)
        Assertions.assertThat(singleTransactionResponseOptional.isPresent)
        val transactionResponse: SingleTransactionResponse = singleTransactionResponseOptional.get()

        // Bob reads the message sent by Alice
        val transactionResponseBob = ccfBob.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(CCFJsonSerializer.fromJson(TestPayloadUser::class.java, transactionResponseBob.get().payload)).isEqualTo(plainObject)

        // Evil reads the message form Alice as well
        val transactionResponseEvil = ccfEvil.getTransactionById(chainIdAlice, transactionResponse.transaction.transactionId)
        Assertions.assertThat(transactionResponseEvil.isEmpty)
    }



    @Test
    @DisplayName("Get Transactions by chainId")
    fun getTransactionsByChainId(){
//        // create two transaction and receive them by chainId
//        val message1 = "Hello Message1: " + OffsetDateTime.now().toString()
//        val message2 = "Hello Message2: " + OffsetDateTime.now().toString()
//
//        val transactionResponse1 = ccfAlice.createTransaction(
//                chainId = chainIdAlice,
//                cryptIdRecipient = bob.cryptId,
//                payload = message1.encodeToByteArray(),
//                plain = true)!!
//
//        val transactionResponse2 = ccfAlice.createTransaction(
//                chainId = chainIdAlice,
//                cryptIdRecipient = bob.cryptId,
//                payload = message2.encodeToByteArray(),
//                plain = true)!!
//
//        val transactions = ccfAlice.getTransactions(chainId = chainIdAlice)

    }

}