package software.catena.ccf.integration.transaction

data class TestPayloadUser(val name: String, val age: Int, val city: String)