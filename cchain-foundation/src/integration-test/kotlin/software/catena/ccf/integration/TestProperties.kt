package software.catena.ccf.integration

import okhttp3.HttpUrl

object TestProperties{

    /*
     *  TODO PW:
     *   - introduce config file for test-server configuration
     *   - execute local tests on local CCM and tests in the pipeline against a remote CCM instance
     */

    fun getCCMConnectionRsa(): HttpUrl{
        return getCCMConnectionRsaLocal()
    }

    private fun getCCMConnectionRsaLocal(): HttpUrl{
        return HttpUrl.parse("https://localhost:443/")!!
    }

//    private fun getCCMConnectionRsaRemote(): HttpUrl{
//        return ""
//    }

//    fun getCCMConnectionEcc(): HttpUrl{
//        return getCCMConnectionEccLocal()
//    }

    private fun getCCMConnectionEccLocal(): HttpUrl{
        return HttpUrl.parse("https://localhost:8443/")!!
    }

//    private fun getCCMConnectionEccRemote(): HttpUrl{
//        return ""
//    }

}