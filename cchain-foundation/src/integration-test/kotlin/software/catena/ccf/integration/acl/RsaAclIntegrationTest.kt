package software.catena.ccf.integration.acl

import software.catena.ccf.CCFService
import software.catena.ccf.crypto.AbstractKeyPair
import software.catena.ccf.crypto.KeyPairFactory
import software.catena.ccf.infrastructure.OkHttpClientFactory
import software.catena.ccf.integration.TestProperties
import software.catena.ccf.models.acl.AclRequest
import software.catena.ccf.models.chain.SingleChainResponse
import software.catena.ccf.models.transaction.SingleTransactionResponse
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime
import java.util.*

@Tag("integrationTest")
class RsaAclIntegrationTest {


    // setup keys for the creator of the chain
    private val alice: AbstractKeyPair = KeyPairFactory.generateKeyPairRsa()
    private val bob: AbstractKeyPair = KeyPairFactory.generateKeyPairRsa()
    private val evil: AbstractKeyPair = KeyPairFactory.generateKeyPairRsa()

    // CCFService that is used for interacting with the CCM
    private  lateinit  var ccfAlice: CCFService
    private  lateinit  var ccfBob: CCFService
    private  lateinit  var ccfEvil: CCFService

    private lateinit var chainIdAlice: UUID

    /*
     * Before each test is executed, the CCFService will be initialized and
     * a chain will be created for further tests.
     */
    @BeforeEach
    @Throws(Exception::class)
    fun setUp() {
        // Create ccf service, alice is always the sender
        ccfAlice = CCFService(
                keyPair = alice,
                httpUrl = TestProperties.getCCMConnectionRsa(),
                clientType = OkHttpClientFactory.ClientType.HTTPS_CA_TRUSTED_BY_SYSTEM,
                security = CCFService.SecurityMode.VERY_FAST
        )

        ccfBob = CCFService(
                keyPair = bob,
                httpUrl = TestProperties.getCCMConnectionRsa(),
                clientType = OkHttpClientFactory.ClientType.HTTPS_CA_TRUSTED_BY_SYSTEM,
                security = CCFService.SecurityMode.VERY_FAST
        )

        ccfEvil = CCFService(
                keyPair = evil,
                httpUrl = TestProperties.getCCMConnectionRsa(),
                clientType = OkHttpClientFactory.ClientType.HTTPS_CA_TRUSTED_BY_SYSTEM,
                security = CCFService.SecurityMode.VERY_FAST
        )

        // Create a new chain for Alice
        chainIdAlice = ccfAlice.createChain(
                name = "Chain of Alice at " + OffsetDateTime.now().toString(),
                description = "This is a chain of Alice",
                closed = false,
                publicRead = false,
                publicWrite = false
        ).get().chain.chainId
    }

    @Test
    @DisplayName("Test to add acl with read permission")
    fun testAddAclRead(){

        val singleChainResponseBob1 = ccfBob.getChainById(chainIdAlice)
        val singleChainResponseEvil1 = ccfEvil.getChainById(chainIdAlice)

        Assertions.assertThat(singleChainResponseBob1.isEmpty)
        Assertions.assertThat(singleChainResponseEvil1.isEmpty)

        val acls: ArrayList<AclRequest> = ArrayList()
        acls.add(AclRequest(
                        publicKey = bob.keyPair.public.encoded,
                        read = true,
                        write = false,
                        deleteAcl = false))

        ccfAlice.replaceAcl(chainIdAlice,acls)

        val singleChainResponseBob2: Optional<SingleChainResponse> = ccfBob.getChainById(chainIdAlice)
        val singleChainResponseEvil2: Optional<SingleChainResponse> = ccfEvil.getChainById(chainIdAlice)

        Assertions.assertThat(singleChainResponseBob2.isPresent)
        Assertions.assertThat(singleChainResponseEvil2.isEmpty)
    }

    @Test
    @DisplayName("Test to add acl with write permission")
    fun testAclWrite(){
        val singleChainResponseBob1 = ccfBob.getChainById(chainIdAlice)
        val singleChainResponseEvil1 = ccfEvil.getChainById(chainIdAlice)

        Assertions.assertThat(singleChainResponseBob1.isEmpty)
        Assertions.assertThat(singleChainResponseEvil1.isEmpty)

        val acls: ArrayList<AclRequest> = ArrayList()
        acls.add(AclRequest(
                publicKey = bob.keyPair.public.encoded,
                read = false,
                write = true,
                deleteAcl = false))
        ccfAlice.replaceAcl(chainIdAlice,acls)

        val payloadBob = "Bob Acl " + OffsetDateTime.now().toString()
        val payloadEvil = "Bob Acl " + OffsetDateTime.now().toString()


        val singleTransactionResponseBob: Optional<SingleTransactionResponse> = ccfBob.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = bob.cryptId,
                payload = payloadBob.toByteArray(),
                plain = true)

        val singleTransactionResponseEvil: Optional<SingleTransactionResponse> = ccfEvil.createTransaction(
                chainId = chainIdAlice,
                cryptIdRecipient = evil.cryptId,
                payload = payloadEvil.toByteArray(),
                plain = true)

        Assertions.assertThat(singleTransactionResponseBob.isPresent)
        Assertions.assertThat(singleTransactionResponseEvil.isEmpty)

    }

    @Test
    @DisplayName("Test update acl")
    fun testUpdateAcl(){

    }

    @Test
    @DisplayName("Test delete acl")
    fun testDeleteAcl(){

    }

    @Test
    @DisplayName("Test get acl")
    fun testGetAcl(){

    }
}