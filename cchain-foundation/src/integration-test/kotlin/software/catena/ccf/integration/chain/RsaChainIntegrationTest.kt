package software.catena.ccf.integration.chain

import software.catena.ccf.CCFService
import software.catena.ccf.crypto.AbstractKeyPair
import software.catena.ccf.crypto.KeyPairFactory
import software.catena.ccf.infrastructure.OkHttpClientFactory
import software.catena.ccf.integration.TestProperties
import software.catena.ccf.models.chain.ChainInfo
import software.catena.ccf.models.chain.ChainsResponse
import software.catena.ccf.models.chain.SingleChainResponse
import org.assertj.core.api.Assertions
import org.junit.Ignore
import org.junit.jupiter.api.*
import java.time.OffsetDateTime
import java.util.*

@Tag("integrationTest")
class RsaChainIntegrationTest {

    // setup keys for the creator of the chain
    private val senderKP: AbstractKeyPair = KeyPairFactory.generateKeyPairRsa()

    // CCFService that is used for interacting with the CCM
    private  lateinit  var ccf: CCFService

    // chain properties
    private lateinit var chainId: UUID
    private val chainName: String = createChainName()
    private val description: String = createChainDescription()
    private val isClosed: Boolean = false
    private val publicRead: Boolean = true
    private val publicWrite: Boolean = true

    private fun createChainName(): String{
        return "Name: Chain created at " + OffsetDateTime.now().toString()
    }

    private fun createChainDescription(): String{
        return "Description: Chain created at " + OffsetDateTime.now().toString()
    }

    /*
     * Before each test is executed, the CCFService will be initialized and
     * a chain will be created for further tests.
     */
    @BeforeEach
    @Throws(Exception::class)
    fun setUp() {
        // Create ccf service
        ccf = CCFService(
                keyPair = senderKP,
                httpUrl = TestProperties.getCCMConnectionRsa(),
                clientType = OkHttpClientFactory.ClientType.HTTPS_CA_TRUSTED_BY_SYSTEM,
                security = CCFService.SecurityMode.VERY_FAST
        )

        // Create a new chain before each test runs
        val chain = ccf.createChain(
                name = this.chainName,
                description = this.description,
                closed = isClosed,
                publicRead = publicRead,
                publicWrite = publicWrite
        ).get().chain

        this.chainId = chain.chainId

    }


    @Test
    @DisplayName("Should create a new chain")
    fun createChain() {
        // Create a new chain to check the response
        val createChainName: String = "createChain (name) test at " + OffsetDateTime.now().toString()
        val createChainDescription: String = "createChain (description) test at " + OffsetDateTime.now().toString()
        val createChainClosed: Boolean = false
        val createChainPublicRead: Boolean = false
        val createChainPublicWrite: Boolean = false
        
        val chainResponseOptional: Optional<SingleChainResponse> = ccf.createChain(
                createChainName,
                createChainDescription,
                createChainClosed,
                createChainPublicRead,
                createChainPublicWrite
        )

        Assertions.assertThat(chainResponseOptional.isPresent)
        val chain = chainResponseOptional.get().chain

        // check that the returned chain and chainId is not null
        Assertions.assertThat(chain).isNotNull
        Assertions.assertThat(chain.chainId).isNotNull

        /*
         *  Check that the chain has the proper values
         */
        // description
        Assertions.assertThat(chain.description).isEqualTo(createChainDescription)
        // name
        Assertions.assertThat(chain.name).isEqualTo(createChainName)
        // closed
        Assertions.assertThat(chain.closed).isEqualTo(createChainClosed)
        // publicRead
        Assertions.assertThat(chain.publicRead).isEqualTo(createChainPublicRead)
        // publicWrite
        Assertions.assertThat(chain.publicWrite).isEqualTo(createChainPublicWrite)
        // public key owner
        /*
         * TODO PW: change chain.publicKeyOwner to PublicKey return type to simplify key use
         *  for this introduce keyType in CCFService.
         */
        Assertions.assertThat(chain.publicKeyOwner).isEqualTo(this.senderKP.keyPair.public.encoded)
    }


    @Test
    @DisplayName("Should receive created chain by chainId")
    @Throws(Exception::class)
    fun getChaiById(){
        // get a chain by its id
        val responseOptional: Optional<SingleChainResponse> = ccf.getChainById(this.chainId)
        Assertions.assertThat(responseOptional.isPresent)
        val response: SingleChainResponse = responseOptional.get()

        // expect the response not to be null
        Assertions.assertThat(response).isNotNull
        Assertions.assertThat(response.chain).isNotNull
        /*
         *  Check that the chain has the proper values
         */
        // description
        Assertions.assertThat(response.chain.description).isEqualTo(this.description)
        // name
        Assertions.assertThat(response.chain.name).isEqualTo(this.chainName)
        // closed
        Assertions.assertThat(response.chain.closed).isEqualTo(this.isClosed)
        // publicRead
        Assertions.assertThat(response.chain.publicRead).isEqualTo(this.publicRead)
        // publicWrite
        Assertions.assertThat(response.chain.publicWrite).isEqualTo(this.publicWrite)
    }

    @Test
    @DisplayName("Should update the chain")
    @Throws(Exception::class)
    fun updateChain(){

        val singleChainResponseOptional: Optional<SingleChainResponse> = ccf.getChainById(this.chainId)
        Assertions.assertThat(singleChainResponseOptional.isPresent)
        val chainInfo: ChainInfo = singleChainResponseOptional.get().chain

        val updatedName: String = createChainName()
        val updatedDescription: String = createChainDescription()
        val updatedClosed: Boolean = !this.isClosed
        val updatedPublicRead: Boolean = !this.publicRead
        val updatedPublicWrite: Boolean = !this.publicWrite

        val chain: ChainInfo = ccf.updateChain(
                chainInfo,
                updatedName,
                updatedDescription,
                updatedClosed,
                updatedPublicRead,
                updatedPublicWrite).get().chain

        /*
         *  Check that the chain has the proper values
         */
        // description
        Assertions.assertThat(chain.description).isEqualTo(updatedDescription)
        // name
        Assertions.assertThat(chain.name).isEqualTo(updatedName)
        // closed
        Assertions.assertThat(chain.closed).isEqualTo(updatedClosed)
        // publicRead
        Assertions.assertThat(chain.publicRead).isEqualTo(updatedPublicRead)
        // publicWrite
        Assertions.assertThat(chain.publicWrite).isEqualTo(updatedPublicWrite)
    }

    @Test
    @DisplayName("A closed chain cannot be updated")
    @Throws(Exception::class)
    fun updateChainWithFailure(){
        // Close chain
        val singleChainResponseOptional: Optional<SingleChainResponse> = ccf.getChainById(this.chainId)
        Assertions.assertThat(singleChainResponseOptional.isPresent)
        val chainInfo: ChainInfo = singleChainResponseOptional.get().chain

        val chainClosed: ChainInfo = ccf.updateChain(chainInfo, closed = true).get().chain
        Assertions.assertThat(chainClosed.closed).isEqualTo(true)

        // Make sure the chain cannot be modified anymode
        val empty: Boolean = ccf.updateChain(chainInfo).isEmpty
        Assertions.assertThat(empty)
    }

    @Test
    @DisplayName("getChainsAll")
    fun getChainsAll(){

//        val response: Optional<ChainsResponse> = ccf.getChains(closed = false)

//        Assertions.assertThat(response.isPresent)
//        Assertions.assertThat(response.get().data).isNotNull
//        Assertions.assertThat(response.get().data.size).isNotEqualTo(0)

    }

    @Ignore
    @Test
    @DisplayName("getChainsOwnerPublicKey")
    fun getChainsOwnerPublicKey(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsNameContains")
    fun getChainsNameContains(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsDescriptionContains")
    fun getChainsDescriptionContains(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsPublicRead")
    fun getChainsPublicRead(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsPublicWrite")
    fun getChainsPublicWrite(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsClosed")
    fun getChainsClosed(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsFromDate")
    fun getChainsFromDate(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsUntilDate")
    fun getChainsUntilDate(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsFromId")
    fun getChainsFromId(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsUntilId")
    fun getChainsUntilId(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsFirst")
    fun getChainsFirst(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsLast")
    fun getChainsLast(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsTransactionSenderPublicKey")
    fun getChainsTransactionSenderPublicKey(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsTransactionRecipientPublicKey")
    fun getChainsTransactionRecipientPublicKey(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsTransactionFromDate")
    fun getChainsTransactionFromDate(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsTransactionUntilDate")
    fun TransactionUntilDate(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsTransactionTypeId")
    fun getChainsTransactionTypeId(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsTransactionMaximumCount")
    fun getChainsTransactionMaximumCount(){
        // TODO PW: implement test
    }

    @Ignore
    @Test
    @DisplayName("getChainsTransactionMinimumCount")
    fun getChainsTransactionMinimumCount(){
        // TODO PW: implement test
    }


}