package serializer

data class TestSerializationObject(val firstName: String, val lastName: String, val age: Int)