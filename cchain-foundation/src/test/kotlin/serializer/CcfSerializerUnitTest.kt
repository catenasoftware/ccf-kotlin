package serializer

import software.catena.ccf.json.CCFJsonSerializer
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unitTest")
class CcfSerializerUnitTest {

    @Test
    fun testSerializationString(){

        val testString = "Hello World"

        val jsonString: String = CCFJsonSerializer.toJson(testString)
        val clazz: String = testString.javaClass.canonicalName

        val testStringDeserialized: Any? = CCFJsonSerializer.fromJson(Class.forName(clazz), jsonString)
        Assertions.assertThat(testStringDeserialized).isEqualTo(testString)
    }

    @Test
    fun testSerializationObject(){
        val user = TestSerializationObject("Max", "Mustermann", 100)

        val jsonString: String = CCFJsonSerializer.toJson(user)
        val clazz: String = user.javaClass.canonicalName

        val userDeserialized: Any? = CCFJsonSerializer.fromJson(Class.forName(clazz), jsonString)
        Assertions.assertThat(userDeserialized).isEqualTo(user)
    }

    @Test
    fun testSerializationObjectToBytes(){
        val user = TestSerializationObject("Max", "Mustermann", 100)

        val jsonBytes: ByteArray = CCFJsonSerializer.toJsonBytesUtf8(user)
        val userDeserialized: TestSerializationObject? = CCFJsonSerializer.fromJson(TestSerializationObject::class.java, jsonBytes)
        Assertions.assertThat(user).isEqualTo(userDeserialized)

    }

}