package crypto

import software.catena.ccf.crypto.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("unitTest")
class RsaKeyPairUnitTest {

    @Test
    @DisplayName("Test ccf key pair with crypt id")
    fun signTextRoundTrip(){

        val keyPairBob: AbstractKeyPair = KeyPairFactory.generateKeyPairRsa()
        val rsaCryptoModuleBob = RsaCryptoModule(keyPairBob.keyPair)

        Assertions.assertThat(keyPairBob.publicKey).isEqualTo(rsaCryptoModuleBob.keyPair.public)
        Assertions.assertThat(keyPairBob.privateKey).isEqualTo(rsaCryptoModuleBob.keyPair.private)

        Assertions.assertThat(keyPairBob.cryptId.publicKey).isEqualTo(rsaCryptoModuleBob.generateCryptId().publicKey)

        // signature includes some randomness
        Assertions.assertThat(keyPairBob.cryptId.publicKeySigned).isNotEqualTo(rsaCryptoModuleBob.generateCryptId().publicKeySigned)
        // but the verification of different signatures must fit
        Assertions.assertThat(rsaCryptoModuleBob.validatePlainBytes(keyPairBob.publicKey.encoded, keyPairBob.cryptId.publicKeySigned))
        Assertions.assertThat(rsaCryptoModuleBob.validatePlainBytes(keyPairBob.publicKey.encoded, rsaCryptoModuleBob.generateCryptId().publicKeySigned))

    }
}