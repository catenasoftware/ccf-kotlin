package crypto

import software.catena.ccf.crypto.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import java.security.SecureRandom

@Tag("unitTest")
class RsaCryptoModuleUnitTest {

    private lateinit var rsaCryptoModule: RsaCryptoModule
    
    @BeforeEach
    @Throws(Exception::class)
    fun setUp() {
        val generator = SecurityProvider.getKeyPairGenerator("RSA")
        generator.initialize(4096, SecureRandom())
        rsaCryptoModule = RsaCryptoModule(generator.generateKeyPair())
    }

    @Test
    @DisplayName("Test sign text round trip")
    fun signTextRoundTrip(){
        val plainText = "Hello World!"
        val signature = rsaCryptoModule.signPlainText(plainText)

        Assertions.assertThat(rsaCryptoModule.validatePlainText(plainText, signature)).isTrue
        Assertions.assertThat(rsaCryptoModule.validatePlainText("Hello World", signature)).isFalse
    }

    @Test
    @DisplayName("Test sign bytes round trip")
    fun signBytesRoundTrip(){
        val plainBytes = "Hello World!".encodeToByteArray()
        val signature = rsaCryptoModule.signPlainBytes(plainBytes)

        Assertions.assertThat(rsaCryptoModule.validatePlainBytes(plainBytes, signature)).isTrue
        Assertions.assertThat(rsaCryptoModule.validatePlainBytes("Hello World".encodeToByteArray(), signature)).isFalse
    }

    @Test
    @DisplayName("Test sign bytes round trip")
    fun singTextTwice(){
        val plainBytes = "Hello World!".encodeToByteArray()

        val signature1 = rsaCryptoModule.signPlainBytes(plainBytes)
        val signature2 = rsaCryptoModule.signPlainBytes(plainBytes)

        Assertions.assertThat(rsaCryptoModule.validatePlainBytes(plainBytes, signature1)).isTrue
        Assertions.assertThat(rsaCryptoModule.validatePlainBytes(plainBytes, signature2)).isTrue
    }

    @Test
    @DisplayName("Validate cryptId")
    fun testValidateCryptId(){
        val cryptId = rsaCryptoModule.generateCryptId()

        Assertions.assertThat(rsaCryptoModule.validatePlainBytes(cryptId.publicKey.encoded, cryptId.publicKeySigned)).isTrue
        Assertions.assertThat(rsaCryptoModule.validatePlainBytes(cryptId.publicKey.encoded, "random".encodeToByteArray())).isFalse
    }

}