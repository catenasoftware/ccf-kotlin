package util

import software.catena.ccf.crypto.KeyHandling
import java.security.cert.X509Certificate

object CCMCertificateAuthority {

    // has to be replaced with own certificate
    val CA = """
-----BEGIN CERTIFICATE-----
MIIEuTCCAyGgAwIBAgIQGwXuiPfFXl5EFeTSF8MNMzANBgkqhkiG9w0BAQsFADB1
MR4wHAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJTAjBgNVBAsMHHNodGVy
ZXYtUENcc3RlcmV2QHNodGVyZXYtUEMxLDAqBgNVBAMMI21rY2VydCBzaHRlcmV2
LVBDXHN0ZXJldkBzaHRlcmV2LVBDMB4XDTIwMDkwOTExNDIzNloXDTMwMDkwOTEx
NDIzNlowdTEeMBwGA1UEChMVbWtjZXJ0IGRldmVsb3BtZW50IENBMSUwIwYDVQQL
DBxzaHRlcmV2LVBDXHN0ZXJldkBzaHRlcmV2LVBDMSwwKgYDVQQDDCNta2NlcnQg
c2h0ZXJldi1QQ1xzdGVyZXZAc2h0ZXJldi1QQzCCAaIwDQYJKoZIhvcNAQEBBQAD
ggGPADCCAYoCggGBAK7mAZXvTiapfIlMf9zqENjbMqzfRPSQFNVr+HtPvH/zYsGY
6onSBRou20FUE/Zz6rvAikfuuUo7lD7Pzb5Bynb/sCHhvrX0ymnUzvgjPMCO6VWk
HQiHgjPKcqqYfnfayGpAQhS5i68hijyikPH1fr3DmsmJrD1neLQ8rl4P4orlTSKR
3mTg4I/v64dckgSlkJgzJPIsr8S9QkQ6y0DrwHUJiQ4XkAD/a9rbq5FDW9c0hLeF
8vBjj7wBxa1HvQBcPCg9hzNWwdCN2vwi0CsKDGZfykeiY36BfSHMWIO9DDE9GPni
x1dy8sATKQCziZt4n6/elxYVqmy9DHdRgUVM/q+9v/Ap6DMvMiXkU+4Irz18ZHEK
/NUfYuuSwoaut8WhsuETsQa6Ksqnaehvb8F6mPrK+GVcNIoLm/7keuviLTl88FFp
k3bbJ9nhdvhb7XYJedP2Y3DYrRWnz4CVoeczyrMFdJ2QiZnstEmNReHnKgx4avhL
L76gdJtymRMr0Kk2DQIDAQABo0UwQzAOBgNVHQ8BAf8EBAMCAgQwEgYDVR0TAQH/
BAgwBgEB/wIBADAdBgNVHQ4EFgQUh6hzI3ByuV7qPJYa/GJksmROgBIwDQYJKoZI
hvcNAQELBQADggGBAB04cdwuxn6xvf9hIW6mPXOiVWe8lY02T1zg1rTs7IM0U5D5
9A4m0ayBefXA1g2pvU7IU7bK9pTuFLuezTQlbetR6R47MmupfqYcXvkKRFBi5n47
BYC6nL+urqRiXFqTS4mIEEKem0hLSEXy4/TZ+tg4xd6m7/Z7zihze65fl8my+1Ny
U/Wd4ES5ybGIo6Uojxt8dICXcAaeChxVl6aiWMpiuhInkgETRjl+oNEzkDDw5zj5
izdPi9kGc3pPSpmj6JujJL1tgqPZJN09/X7k/socnY0eZnGYM+JycD2VCeOWvOH3
sKVmKHpwcQE2HjPvNmEJuAqfziyIDfz9Ao0P8Wza4QEHvnGio3g4ApZfP2QQZCyy
2N6j9TIGekz5iCg65yqb8uQOB18LX2mbRDwP36w9OsosBpAh2Izwpjbm7M2jH0nw
TsOerg+jQ3lquqM41ZY38Od2TnJhEmOSGPIoizdJRg/T1ZjoFSy+7jRpyoStNFJW
jL5yVORwcRK5j/sAwQ==
-----END CERTIFICATE-----
    """.trimIndent()

    val CCM_CA = listOf<X509Certificate>(
        KeyHandling.getCertificateFromPem(CA)
    )

    /*
    // content of file has to be own certificate
    val CCM_CA = listOf<X509Certificate>(
        KeyHandling.getCertificateFromPemFile(File("src/test/kotlin/util/rootCA.pem").absolutePath)
    )*/
}