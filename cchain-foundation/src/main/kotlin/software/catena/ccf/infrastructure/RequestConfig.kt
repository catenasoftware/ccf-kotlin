package software.catena.ccf.infrastructure

import com.google.common.base.Joiner
import java.security.PublicKey
import java.util.*

/**
 * Defines a config object for a given request.
 * NOTE: This object doesn't include 'body' because it
 *       allows for caching of the constructed object
 *       for many request definitions.
 * NOTE: Headers is a Map<String,String> because rfc2616 defines
 *       multi-valued headers as csv-only.
 */
data class RequestConfig(
    val method: RequestMethod,
    val route: ApiRoute
){
    /**
     * Provides enumerated HTTP verbs
     */
    enum class RequestMethod {
        GET, DELETE, POST, PUT
    }
    /**
     *  Headers
     */
    val headers: MutableMap<String, String> = mutableMapOf()

    fun addHeaderXApiSession(value: UUID): RequestConfig{
        headers["X-API-SessionToken"] = value.toString()
        return this
    }

    /**
     *  Query Parameters
     */
    val queryParam: MutableMap<String, List<String>> = mutableMapOf()

    private fun isNotNull(o: Any?): Boolean{
        return o != null
    }

    fun addQueryParameter(key: String, value: String?): RequestConfig{
        if(isNotNull(value)){
            queryParam[key] = listOf(value!!)
        }
        return this
    }

    fun addQueryParameter(key: String, value: java.time.OffsetDateTime?): RequestConfig{
        return addQueryParameter(key, value?.toString())
    }

    fun addQueryParameter(key: String, value: UUID?): RequestConfig{
        return addQueryParameter(key, value?.toString())
    }

    fun addQueryParameter(key: String, value: Boolean?): RequestConfig{
        return addQueryParameter(key, value?.toString())
    }

    fun addQueryParameter(key: String, value: PublicKey?): RequestConfig{
        return addQueryParameter(key, toRequestParamByteArray(value))
    }

    fun addQueryParameter(key: String, value: Long?): RequestConfig{
        return addQueryParameter(key, value?.toString())
    }

    fun addQueryParameter(key: String, value: Int?): RequestConfig{
        return addQueryParameter(key, value?.toString())
    }

    //TODO in CCM-Go?: Base64.getEncoder().encodeToString(array)
    private fun toRequestParamByteArray (value: PublicKey?): String? {
        return if (value != null) {
            Joiner.on(",").join(value.encoded.iterator())
        } else {
            null
        }
    }
}