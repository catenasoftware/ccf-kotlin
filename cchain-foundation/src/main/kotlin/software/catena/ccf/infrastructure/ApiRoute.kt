package software.catena.ccf.infrastructure

import java.lang.StringBuilder
import java.util.*

class ApiRoute {

    private val stringBuilder: StringBuilder = StringBuilder()

    fun root(): ApiRoute{
        stringBuilder.append("/")
        return this
    }

    fun chains(): ApiRoute{
        stringBuilder.append("/chains")
        return this
    }

    fun chainId(uuid: UUID): ApiRoute{
        return uuid(uuid)
    }

    fun transactions(): ApiRoute{
        stringBuilder.append("/transactions")
        return this
    }

    fun transactionId(uuid: UUID): ApiRoute{
        return uuid(uuid)
    }

    fun session(): ApiRoute{
        stringBuilder.append("/session")
        return this
    }

    fun sessionToken(uuid: UUID): ApiRoute{
        return uuid(uuid)
    }

    fun connecttoken(): ApiRoute{
        stringBuilder.append("/connecttoken")
        return this
    }

    fun acl(): ApiRoute{
        stringBuilder.append("/acl")
        return this
    }

    private fun uuid(uuid: UUID): ApiRoute{
        stringBuilder.append("/$uuid")
        return this
    }

    fun getRoute(): String{
        return stringBuilder.toString()
    }

}