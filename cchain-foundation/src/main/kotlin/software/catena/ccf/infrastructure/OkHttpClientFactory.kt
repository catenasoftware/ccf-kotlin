package software.catena.ccf.infrastructure

import software.catena.ccf.CCFService
import software.catena.ccf.crypto.CertificateAuthorities
import software.catena.ccf.utils.FoundationException
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.tls.HandshakeCertificates
import java.net.URL
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.*


class OkHttpClientFactory {

    enum class ClientType(val value: Int) {
        /**
         *  HTTPS client which has an unsafe trust manager with no certificate validation,
         *  only for testing otherwise prone to MiM Attacks
         **/
        HTTPS_CA_NO_CHECK(-1),

        /**
         *  HTTPS client which has a trust manager that trusts all CAs trusted by the system,
         *  in addition with the CAs of Let's Encrypt, Comodo, Entrust (normally also trusted by the system).
         *  Own CAs could be also added for testing.
         *
         *  This is a good option when testing on Android with an own CA certificate, also self signed.
         *  The certificate has to be also bound in the App:
         *  https://developer.android.com/training/articles/security-config#ConfigCustom
         **/
        HTTPS_CA_TRUSTED_BY_SYSTEM(0),

        /**
         *  HTTPS client which has a trust manager that trusts the CAs of Let's Encrypt, Comodo, Entrust.
         *  This is the safest option and is the default one. Own CAs could be also added for testing.
         *
         **/
        HTTPS_CA_LIMITED_SET(1),

        /**
         *  HTTP client
         **/
        HTTP(2)
    }

    companion object {

        private var okHttpClient: OkHttpClient? = null

        fun getOkHttpClient(url: String, clientType: ClientType): OkHttpClient {

            if (okHttpClient != null)
                return okHttpClient!!

            okHttpClient = OkHttpClient()

//            val https: Boolean = URL(url).protocol == "https"

            return when (clientType) {
                ClientType.HTTPS_CA_NO_CHECK -> {
//                    if (!https)
//                        throw FoundationException("$url is not a https protocol")

                    getUnsafeClient()
                }
                ClientType.HTTPS_CA_TRUSTED_BY_SYSTEM -> {
//                    if (!https)
//                        throw FoundationException("$url is not a https protocol")

                    getTrustedCertificatesClient(true)
                }
                ClientType.HTTPS_CA_LIMITED_SET -> {
//                    if (!https)
//                        throw FoundationException("$url is not a https protocol")

                    getTrustedCertificatesClient()
                }
                ClientType.HTTP -> {
//                    if (https)
//                        throw FoundationException("$url is not a http protocol")

                    getClearTextClient()
                }
            }
        }

        private fun getClearTextClient(): OkHttpClient {
            return try {
                return okHttpClient!!.newBuilder()
                    .connectionSpecs(
                        listOf(ConnectionSpec.CLEARTEXT)
                    )
                    .build()
            } catch (e: Exception) {
                throw FoundationException("Error when instantiating clear text okHttp client (message = '${e.message}')")
            }
        }

        private fun getTrustedCertificatesClient(platformTrusted: Boolean = false, ownCA: List<X509Certificate>? = null): OkHttpClient {
            try {

                val certificates: HandshakeCertificates =
                    HandshakeCertificates.Builder().apply {

                        CertificateAuthorities.CA.forEach { addTrustedCertificate(it) }
                        // Add own CAs
                        ownCA?.forEach { addTrustedCertificate(it) }

                        // standard certificates are also required.
                        if (platformTrusted)
                            addPlatformTrustedCertificates()

                    }.build()

                return okHttpClient!!.newBuilder()
                    .sslSocketFactory(certificates.sslSocketFactory(),
                        certificates.trustManager())
                    .build()
            } catch (e: Exception) {
                throw FoundationException("Error when instantiating trusted certificates okHttp client (message = '${e.message}')")
            }
        }

        private fun getUnsafeClient(): OkHttpClient {
            return try {
                val trustAllCerts: Array<TrustManager> = arrayOf<TrustManager>(
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate>,
                            authType: String,
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate>,
                            authType: String,
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate> {
                            return arrayOf()
                        }
                    }
                )
                val sslContext: SSLContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())
                val sslSocketFactory: SSLSocketFactory = sslContext.socketFactory
                val builder = okHttpClient!!.newBuilder()
                builder.sslSocketFactory(
                    sslSocketFactory,
                    (trustAllCerts[0] as X509TrustManager)
                )
                builder.hostnameVerifier(HostnameVerifier { _, _ -> true })
                builder //.addInterceptor(interceptor)
                    .build()
            } catch (e: Exception) {
                throw FoundationException("Error when instantiating unsafe okHttp client (message = '${e.message}')")
            }
        }
    }


}