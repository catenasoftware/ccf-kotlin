package software.catena.ccf.infrastructure

import software.catena.ccf.CCFService
import software.catena.ccf.crypto.*
import software.catena.ccf.utils.FoundationException
import software.catena.ccf.utils.Logger
import okhttp3.HttpUrl
import java.security.cert.X509Certificate

data class ServiceConfiguration(val keyPair: AbstractKeyPair,
                                val httpUrl: HttpUrl,
                                val securityMode: CCFService.SecurityMode,
                                val httpClientType: OkHttpClientFactory.ClientType,
                                val ownCA: List<X509Certificate>?,
                                val verbosity: Logger.VerbosityMode,
                                val logger: Logger?){


    val cryptoModule: AsymmetricCryptoModule = when (keyPair) {
        is RsaKeyPair -> RsaCryptoModule(keyPair.keyPair)
        else -> throw FoundationException("Unsupported CCF Key Pair!")
    }

}