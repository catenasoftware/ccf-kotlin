package software.catena.ccf

import okhttp3.HttpUrl
import software.catena.ccf.crypto.AbstractKeyPair
import software.catena.ccf.crypto.CryptId
import software.catena.ccf.infrastructure.OkHttpClientFactory
import software.catena.ccf.infrastructure.ServiceConfiguration
import software.catena.ccf.models.acl.AclRequest
import software.catena.ccf.models.acl.AclResponse
import software.catena.ccf.models.chain.ChainInfo
import software.catena.ccf.models.chain.SingleChainResponse
import software.catena.ccf.models.session.ServerPublicKeySignature
import software.catena.ccf.models.transaction.SingleTransactionResponse
import software.catena.ccf.models.transaction.Transaction
import software.catena.ccf.services.AclService
import software.catena.ccf.services.ChainsService
import software.catena.ccf.services.SessionService
import software.catena.ccf.services.TransactionsService
import software.catena.ccf.utils.Logger
import java.security.PublicKey
import java.security.cert.X509Certificate
import java.util.*

class CCFService constructor(
        private val keyPair: AbstractKeyPair,
        private val httpUrl: HttpUrl,
        private val clientType: OkHttpClientFactory.ClientType = OkHttpClientFactory.ClientType.HTTPS_CA_LIMITED_SET,
        private val ownCA: List<X509Certificate>? = null,
        private val security: SecurityMode = SecurityMode.SAFE,
        private val verbosity: Logger.VerbosityMode = Logger.VerbosityMode.LOGGER,
) {

    enum class SecurityMode(val value: Int) {
        VERY_SAFE(-1), // signature validation in GET requests; besides CCM's also the sender's signature is validated
        SAFE(0), // default mode, CCM's signature validation on
        FAST(1),// fast, unsafe mode, no signature validation
        VERY_FAST(2)// fast, unsafe mode, no signature validation, no plausibility checks
    }

    private val serviceConfiguration: ServiceConfiguration = ServiceConfiguration(
            keyPair = this.keyPair,
            httpUrl = this.httpUrl,
            securityMode = this.security,
            httpClientType = this.clientType,
            ownCA = this.ownCA,
            verbosity = this.verbosity,
            logger = Logger.getInstance(this.verbosity))


    private val chainsService: ChainsService
    private var transactionsService: TransactionsService
    private var aclService: AclService
    private var sessionService: SessionService

    init {
        sessionService = SessionService(this.serviceConfiguration)
        chainsService = ChainsService(this.serviceConfiguration, this.sessionService)
        transactionsService = TransactionsService(this.serviceConfiguration, this.sessionService)
        aclService = AclService(this.serviceConfiguration, this.sessionService)
    }

    /**
     *  Create a new chain
     *
     * @param name Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long.
     * @param description Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long.
     * @param closed If closed no one can write into chain. The chain owner only can change the state.
     * @param publicRead
     * @param publicWrite
     */
    fun createChain(
            /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long. */
            name: String,
            /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long. */
            description: String,
            /* If closed no one can write into chain. The chain owner only can change the state. */
            closed: Boolean = false,
            publicRead: Boolean = true,
            publicWrite: Boolean = true,

            ): Optional<SingleChainResponse> {

        return chainsService.createChain(name, description, closed, publicRead, publicWrite)
    }

//    /**
//     * Create a new chain asynchronously
//     *
//     * @param name Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long.
//     * @param description Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long.
//     * @param closed If closed no one can write into chain. The chain owner only can change the state.
//     * @param publicRead
//     * @param publicWrite
//     */
//     suspend fun createChainAsync(
//            /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long. */
//            name: String,
//            /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long. */
//            description: String,
//            /* If closed no one can write into chain. The chain owner only can change the state. */
//            closed: Boolean = false,
//            publicRead: Boolean = true,
//            publicWrite: Boolean = true,
//    ): Optional<SingleChainResponse> = coroutineScope {
//        val deferred = async {
//            chainsService.createChain(name,
//                    description,
//                    closed,
//                    publicRead,
//                    publicWrite)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }

    /**
     * Update an existing chain
     *
     * @param chain the chain to be updated
     * @param name Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long.
     * @param description Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long.
     * @param closed If closed no one can write into chain. The chain owner only can change the state.
     * @param publicRead
     * @param publicWrite
     *
     * @return a new object with updated values
     */
    fun updateChain(
            chain: ChainInfo,
            /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long. */
            name: String? = null,
            /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long. */
            description: String? = null,
            closed: Boolean? = null,
            publicRead: Boolean? = null,
            publicWrite: Boolean? = null,
    ): Optional<SingleChainResponse> {

        return chainsService.updateChain(chain,
                name,
                description,
                closed,
                publicRead,
                publicWrite)
    }

//    /**
//     * Update an existing chain asynchronously
//     *
//     * @param chain the chain to be updated
//     * @param name Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long.
//     * @param description Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long.
//     * @param closed If closed no one can write into chain. The chain owner only can change the state.
//     * @param publicRead
//     * @param publicWrite
//     */
//     suspend fun updateChainAsync(
//            chain: ChainInfo,
//            /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long. */
//            name: String? = null,
//            /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except control characters. Maximum 255 bytes long. */
//            description: String? = null,
//            /* If closed no one can write into chain. The chain owner only can change the state. */
//            closed: Boolean? = null,
//            publicRead: Boolean? = null,
//            publicWrite: Boolean? = null,
//    ): Optional<SingleChainResponse> = coroutineScope {
//        val deferred = async {
//            chainsService.updateChain(chain,
//                    name,
//                    description,
//                    closed,
//                    publicRead,
//                    publicWrite)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }


    /**
     * get a specific chain
     * Returns the details of a specific chain by chainId.
     * @param chainId
     * @return ChainInfoTxRequestDataToHash
     */
    fun getChainById(chainId: UUID): Optional<SingleChainResponse> {
        return chainsService.getChainById(chainId)
    }

//    /**
//     * get a specific chain asynchronously
//     * Returns the details of a specific chain by chainId.
//     * @param chainId
//     * @return ChainInfoTxRequestDataToHash
//     */
//    suspend fun getChainByIdAsync(chainId: UUID): Optional<SingleChainResponse> = coroutineScope {
//        val deferred = async {
//            chainsService.getChainById(chainId)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }

//    /**
//     * returns an array of chains
//     * Returns by default all chains managed by the CCM.
//     *
//     * @param ownerPublicKey Returns chains the given public keys is the chain owner. (optional)
//     * @param nameContains Returns chains where the name contains the given string. (optional)
//     * @param descriptionContains Returns chains where the description contains the given string. (optional)
//     * @param publicRead Returns chains which are either readable by everyone or not. (optional)
//     * @param publicWrite Returns chains which are either writeable by everyone or not. (optional)
//     * @param closed Returns chains which are either open or closed. (optional)
//     * @param fromDate returns items created after the given date-time. (optional)
//     * @param untilDate returns items created before the given date-time. (optional)
//     * @param fromId Returns all items after the given UUID. (optional)
//     * @param untilId Returns all items until the given UUID. (optional)
//     * @param first [NOT IMPLEMENTED!] returns the first n items. (optional)
//     * @param last [NOT IMPLEMENTED!] returns the last n items. (optional)
//     * @param transactionSenderPublicKey [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given sender public key. (optional)
//     * @param transactionRecipientPublicKey [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given recipient public key. (optional)
//     * @param transactionFromDate [NOT IMPLEMENTED!] Returns all chains containing transactions after the given date-time. (optional)
//     * @param transactionUntilDate [NOT IMPLEMENTED!] Returns all chains containing transactions before the given date-time. (optional)
//     * @param transactionTypeId [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given typeId. (optional)
//     * @param transactionMaximumCount [NOT IMPLEMENTED!] Returns all chains containing maximum n transactions. (optional)
//     * @param transactionMinimumCount [NOT IMPLEMENTED!] Returns all chains containing minimum n transactions. (optional)
//     * @return ChainsResponse
//     */
//    fun getChains(
//            ownerPublicKey: PublicKey? = null,
//            nameContains: String? = null,
//            descriptionContains: String? = null,
//            publicRead: Boolean? = null,
//            publicWrite: Boolean? = null,
//            closed: Boolean? = null,
//            fromDate: OffsetDateTime? = null,
//            untilDate: OffsetDateTime? = null,
//            fromId: UUID? = null,
//            untilId: UUID? = null,
//            first: Long? = null,
//            last: Long? = null,
//            transactionSenderPublicKey: PublicKey? = null,
//            transactionRecipientPublicKey: PublicKey? = null,
//            transactionFromDate: OffsetDateTime? = null,
//            transactionUntilDate: OffsetDateTime? = null,
//            transactionTypeId: Int? = null,
//            transactionMaximumCount: Long? = null,
//            transactionMinimumCount: Long? = null,
//    ): Optional<ChainsResponse> {
//
//        return chainsService.getChains(ownerPublicKey,
//                nameContains,
//                descriptionContains,
//                publicRead,
//                publicWrite,
//                closed,
//                fromDate,
//                untilDate,
//                fromId,
//                untilId,
//                first,
//                last,
//                transactionSenderPublicKey,
//                transactionRecipientPublicKey,
//                transactionFromDate,
//                transactionUntilDate,
//                transactionTypeId,
//                transactionMaximumCount,
//                transactionMinimumCount)
//
//    }

//    /**
//     * returns an array of chains asynchronously
//     * Returns by default all chains managed by the CCM.
//     *
//     * @param ownerPublicKey Returns chains the given public keys is the chain owner. (optional)
//     * @param nameContains Returns chains where the name contains the given string. (optional)
//     * @param descriptionContains Returns chains where the description contains the given string. (optional)
//     * @param publicRead Returns chains which are either readable by everyone or not. (optional)
//     * @param publicWrite Returns chains which are either writeable by everyone or not. (optional)
//     * @param closed Returns chains which are either open or closed. (optional)
//     * @param fromDate returns items created after the given date-time. (optional)
//     * @param untilDate returns items created before the given date-time. (optional)
//     * @param fromId Returns all items after the given UUID. (optional)
//     * @param untilId Returns all items until the given UUID. (optional)
//     * @param first [NOT IMPLEMENTED!] returns the first n items. (optional)
//     * @param last [NOT IMPLEMENTED!] returns the last n items. (optional)
//     * @param transactionSenderPublicKey [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given sender public key. (optional)
//     * @param transactionRecipientPublicKey [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given recipient public key. (optional)
//     * @param transactionFromDate [NOT IMPLEMENTED!] Returns all chains containing transactions after the given date-time. (optional)
//     * @param transactionUntilDate [NOT IMPLEMENTED!] Returns all chains containing transactions before the given date-time. (optional)
//     * @param transactionTypeId [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given typeId. (optional)
//     * @param transactionMaximumCount [NOT IMPLEMENTED!] Returns all chains containing maximum n transactions. (optional)
//     * @param transactionMinimumCount [NOT IMPLEMENTED!] Returns all chains containing minimum n transactions. (optional)
//     * @return ChainsResponse
//     */
//    suspend fun getChainsAsync(
//            ownerPublicKey: PublicKey? = null,
//            nameContains: String? = null,
//            descriptionContains: String? = null,
//            publicRead: Boolean? = null,
//            publicWrite: Boolean? = null,
//            closed: Boolean? = null,
//            fromDate: OffsetDateTime? = null,
//            untilDate: OffsetDateTime? = null,
//            fromId: UUID? = null,
//            untilId: UUID? = null,
//            first: Long? = null,
//            last: Long? = null,
//            transactionSenderPublicKey: PublicKey? = null,
//            transactionRecipientPublicKey: PublicKey? = null,
//            transactionFromDate: OffsetDateTime? = null,
//            transactionUntilDate: OffsetDateTime? = null,
//            transactionTypeId: Int? = null,
//            transactionMaximumCount: Long? = null,
//            transactionMinimumCount: Long? = null,
//    ): Optional<ChainsResponse> = coroutineScope {
//        val deferred = async {
//            chainsService.getChains(ownerPublicKey,
//                    nameContains,
//                    descriptionContains,
//                    publicRead,
//                    publicWrite,
//                    closed,
//                    fromDate,
//                    untilDate,
//                    fromId,
//                    untilId,
//                    first,
//                    last,
//                    transactionSenderPublicKey,
//                    transactionRecipientPublicKey,
//                    transactionFromDate,
//                    transactionUntilDate,
//                    transactionTypeId,
//                    transactionMaximumCount,
//                    transactionMinimumCount)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }


    /**
     * Creates a new transaction in the given chain.
     *
     * @param chainId
     * @param cryptIdRecipient
     * @param typeId typeId 2 and 3 is reserved for ACL requests, 4-29 is reserved for future system use, 30 is a default transaction, >30 can be used by clients to distinguish between transaction types.
     * @param plain
     * @param payload
     * @param noReply Don't send signed transaction back. Only HTTP 201 (optional, default to false)
     *
     * @return TransactionResponse
     */
    fun createTransaction(
            chainId: UUID,
            cryptIdRecipient: CryptId,
            payload: ByteArray,
            plain: Boolean = false,
            typeId: Int = Transaction.TransactionType.DEFAULT.value,
            noReply: Boolean = false,
    ): Optional<SingleTransactionResponse> {

        return transactionsService.createTransaction(
                chainId = chainId,
                cryptIdRecipient = cryptIdRecipient,
                payload = payload,
                plain = plain,
                typeId = typeId,
                noReply = noReply)
    }

//    /**
//     * Creates a new transaction asynchronously in the given chain.
//     *
//     * @param chainId
//     * @param cryptIdRecipient
//     * @param typeId typeId 2 and 3 is reserved for ACL requests, 4-29 is reserved for future system use, 30 is a default transaction, >30 can be used by clients to distinguish between transaction types.
//     * @param plain
//     * @param payload
//     * @param noReply Don't send signed transaction back. Only HTTP 201 (optional, default to false)
//     */
//    suspend fun createTransactionAsync(
//            chainId: UUID,
//            cryptIdRecipient: CryptId,
//            payload: ByteArray,
//            plain: Boolean = false,
//            typeId: Int = Transaction.TransactionType.DEFAULT.value,
//            noReply: Boolean = false,
//    ): Optional<SingleTransactionResponse> = coroutineScope {
//        val deferred = async {
//            transactionsService.createTransaction(
//                    chainId = chainId,
//                    cryptIdRecipient = cryptIdRecipient,
//                    payload = payload,
//                    plain = plain,
//                    typeId = typeId,
//                    noReply = noReply)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }


    /**
     * returns specific transaction
     * Returns a specific transaction given the chainId and transactionId.
     * @param chainId
     * @param transactionId
     * @return TransactionRequestDataToHash
     */
    fun getTransactionById(chainId: UUID, transactionId: UUID): Optional<SingleTransactionResponse> {
        return transactionsService.getTransactionById(chainId, transactionId)
    }

//    /**
//     * returns specific transaction asynchronously
//     * Returns a specific transaction given the chainId and transactionId.
//     * @param chainId
//     * @param transactionId
//     */
//    suspend fun getTransactionByIdAsync(chainId: UUID, transactionId: UUID): Optional<SingleTransactionResponse> = coroutineScope {
//        val deferred = async {
//            transactionsService.getTransactionById(chainId, transactionId)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }

//    /**
//     * Returns by default all transactions of all chains.
//     *
//     * @param chainId  (optional)
//     * @param senderPublicKey Returns transactions matching the given senders public key. (optional)
//     * @param recipientPublicKey Returns transactions matching the given recipient public key. (optional)
//     * @param typeId Returns transactions matching the given typeId. (optional)
//     * @param fromDate Returns transactions created after the given date-time. (optional)
//     * @param untilDate Returns transactions created before the given date-time. (optional)
//     * @param first [NOT IMPLEMENTED!] Returns the i first transactions. (optional)
//     * @param untilId Returns all transactions until the given UUID. (optional)
//     * @param fromId Returns all transactions after the given UUID. (optional)
//     * @param last [NOT IMPLEMENTED!] Returns the last i transactions. (optional)
//     * @return TransactionResponse
//     */
//    fun getTransactions(
//            chainId: UUID? = null,
//            senderPublicKey: PublicKey? = null,
//            recipientPublicKey: PublicKey? = null,
//            typeId: Int? = null,
//            fromDate: OffsetDateTime? = null,
//            untilDate: OffsetDateTime? = null,
//            fromId: UUID? = null,
//            untilId: UUID? = null,
//            first: Long? = null,
//            last: Long? = null,
//    ): Optional<TransactionResponse> {
//
//        return transactionsService.getTransactions(chainId,
//                senderPublicKey,
//                recipientPublicKey,
//                typeId,
//                fromDate,
//                untilDate,
//                fromId,
//                untilId,
//                first,
//                last)
//    }

//    /**
//     * Returns by default all transactions of all chains.
//     *
//     * @param chainId  (optional)
//     * @param senderPublicKey Returns transactions matching the given senders public key. (optional)
//     * @param recipientPublicKey Returns transactions matching the given recipient public key. (optional)
//     * @param typeId Returns transactions matching the given typeId. (optional)
//     * @param fromDate Returns transactions created after the given date-time. (optional)
//     * @param untilDate Returns transactions created before the given date-time. (optional)
//     * @param first [NOT IMPLEMENTED!] Returns the i first transactions. (optional)
//     * @param untilId Returns all transactions until the given UUID. (optional)
//     * @param fromId Returns all transactions after the given UUID. (optional)
//     * @param last [NOT IMPLEMENTED!] Returns the last i transactions. (optional)
//     * @return TransactionResponse
//     */
//    suspend fun getTransactionsAsync(
//            chainId: UUID? = null,
//            senderPublicKey: PublicKey? = null,
//            recipientPublicKey: PublicKey? = null,
//            typeId: Int? = null,
//            fromDate: OffsetDateTime? = null,
//            untilDate: OffsetDateTime? = null,
//            fromId: UUID? = null,
//            untilId: UUID? = null,
//            first: Long? = null,
//            last: Long? = null,
//    ): Optional<TransactionResponse> = coroutineScope {
//        val deferred = async {
//            transactionsService.getTransactions(chainId,
//                    senderPublicKey,
//                    recipientPublicKey,
//                    typeId,
//                    fromDate,
//                    untilDate,
//                    fromId,
//                    untilId,
//                    first,
//                    last)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }

    /**
     * Updates an existing ACL for the given chain. New entries are added
     * incrementally, or when already existing, updated or deleted.
     *
     * @param chainId the chain this ACL belongs to
     * @param aclList The payload must contain a list of AclRequest elements.
     * @return TransactionResponse
     */
    fun updateAcl(chainId: UUID, aclList: ArrayList<AclRequest> = arrayListOf()): Optional<SingleTransactionResponse> {
        return aclService.updateAcl(chainId, aclList)
    }

    /**
     * Updates an existing ACL for the given chain. A new entry is added,
     * or when already existing, then it is updated or deleted.
     *
     * @param chainId the chain this ACL belongs to
     * @param publicKey PublicKey of the user the ACL is assigned to. Format in DER (ASN.1).
     * @param read If null publicRead is used otherwise value overrides publicRead value.
     * @param write If null publicWrite is used otherwise value overrides publicWrite value.
     * @param deleteAcl Delete ACL entry for given publicKey. Obsoletes other parameters.
     * @return TransactionResponse
     */
    fun updateAcl(
            chainId: UUID,
            publicKey: ByteArray,
            read: Boolean? = null,
            write: Boolean? = null,
            deleteAcl: Boolean? = null,
    ): Optional<SingleTransactionResponse> {

        return aclService.updateAcl(chainId, publicKey, read, write, deleteAcl)
    }


//    /**
//     * Updates asynchronously an existing ACL for the given chain. New entries are added
//     * incrementally, or when already existing, then updated or deleted.
//     *
//     * @param chainId the chain this ACL belongs to
//     * @param aclList The payload must contain a list of AclRequest elements.
//     */
//    suspend fun updateAclAsync(
//            chainId: UUID,
//            aclList: ArrayList<AclRequest> = arrayListOf(),
//    ): Optional<SingleTransactionResponse> = coroutineScope {
//        val deferred = async {
//            aclService.updateAcl(chainId, aclList)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }


//    /**
//     * Updates asynchronously an existing ACL for the given chain. A new entry is added,
//     * or when already existing, then it is updated or deleted.
//     *
//     * @param chainId the chain this ACL belongs to
//     * @param publicKey PublicKey of the user the ACL is assigned to. Format in DER (ASN.1).
//     * @param read If null publicRead is used otherwise value overrides publicRead value.
//     * @param write If null publicWrite is used otherwise value overrides publicWrite value.
//     * @param deleteAcl Delete ACL entry for given publicKey. Obsoletes other parameters.
//     */
//    suspend fun updateAclAsync(
//            chainId: UUID,
//            publicKey: ByteArray,
//            read: Boolean? = null,
//            write: Boolean? = null,
//            deleteAcl: Boolean? = null,
//    ): Optional<SingleTransactionResponse> = coroutineScope {
//        val deferred = async {
//            aclService.updateAcl(chainId, publicKey, read, write, deleteAcl)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }

    /**
     * Creates a new ACL for the given chain. The old ACL for the whole chain is deleted.
     * New entries are added incrementally if a list of AclRequest is specified.
     *
     * @param chainId the chain this ACL belongs to
     * @param aclList The payload could contain a list of AclRequest elements.
     * @return TransactionResponse
     */
    fun replaceAcl(
            chainId: UUID,
            aclList: ArrayList<AclRequest> = arrayListOf(),
    ): Optional<SingleTransactionResponse> {

        return aclService.replaceAcl(chainId, aclList)
    }


    /**
     * Creates a new ACL for the given chain. The old ACL for the whole chain is deleted,
     * a new entry is then added.
     *
     * @param chainId the chain this ACL belongs to
     * @param publicKey PublicKey of the user the ACL is assigned to. Format in DER (ASN.1).
     * @param read If null publicRead is used otherwise value overrides publicRead value.
     * @param write If null publicWrite is used otherwise value overrides publicWrite value.
     * @return TransactionResponse
     */
    fun replaceAcl(
            chainId: UUID,
            publicKey: ByteArray,
            read: Boolean? = null,
            write: Boolean? = null,
    ): Optional<SingleTransactionResponse> {

        return aclService.replaceAcl(chainId, publicKey, read, write)
    }


//    /**
//     * Creates a new ACL for the given chain asynchronously. The old ACL for the whole chain is deleted.
//     * New entries are added incrementally if a list of AclRequest is specified.
//     *
//     * @param chainId the chain this ACL belongs to
//     * @param aclList The payload could contain a list of AclRequest elements.
//     */
//    suspend fun replaceAclAsync(
//            chainId: UUID,
//            aclList: ArrayList<AclRequest> = arrayListOf(),
//    ): Optional<SingleTransactionResponse> = coroutineScope {
//        val deferred = async {
//            aclService.replaceAcl(chainId, aclList)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }

//    /**
//     * Creates a new ACL for the given chain asynchronously. The old ACL for the whole chain is deleted
//     * and a new entry is added.
//     *
//     * @param chainId the chain this ACL belongs to
//     * @param publicKey PublicKey of the user the ACL is assigned to. Format in DER (ASN.1).
//     * @param read If null publicRead is used otherwise value overrides publicRead value.
//     * @param write If null publicWrite is used otherwise value overrides publicWrite value.
//     */
//    suspend fun replaceAclAsync(
//            chainId: UUID,
//            publicKey: ByteArray,
//            read: Boolean? = null,
//            write: Boolean? = null,
//    ): Optional<SingleTransactionResponse>  = coroutineScope {
//        val deferred = async {
//            aclService.replaceAcl(chainId, publicKey, read, write)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }


    /**
     * Deletes the ACL for the given chain.
     *
     * @param chainId the chain this ACL belongs to
     * @return TransactionResponse
     */
    fun deleteAcl(chainId: UUID): Optional<SingleTransactionResponse> {
        return aclService.replaceAcl(chainId, arrayListOf())
    }

//    /**
//     * Deletes the ACL for the given chain asynchronously.
//     *
//     * @param chainId the chain this ACL belongs to
//     */
//    suspend fun deleteAclAsync(
//            chainId: UUID,
//    ): Optional<SingleTransactionResponse> = coroutineScope {
//        val deferred = async {
//            aclService.replaceAcl(chainId, arrayListOf())
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }

    /**
     * Deletes an existing ACL entry for the given chain.
     *
     * @param chainId the chain this ACL belongs to
     * @param publicKey PublicKey of the user the ACL is assigned to. Format in DER (ASN.1).
     * @return TransactionResponse
     */
    fun deleteAcl(
            chainId: UUID,
            publicKey: ByteArray,
    ): Optional<SingleTransactionResponse> {

        return aclService.updateAcl(chainId, publicKey, deleteAcl = true)
    }


//    /**
//     * Deletes asynchronously an existing ACL entry for the given chain.
//     *
//     * @param chainId the chain this ACL belongs to
//     * @param publicKey PublicKey of the user the ACL is assigned to. Format in DER (ASN.1).
//     */
//    suspend fun deleteAclAsync(
//            chainId: UUID,
//            publicKey: ByteArray,
//    ): Optional<SingleTransactionResponse> = coroutineScope {
//        val deferred = async {
//            aclService.updateAcl(chainId, publicKey, deleteAcl = true)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }


    /**
     * Returns by default the ACL of the session owner for the given chain.
     * Parameters showAll and publicKey cannot be used together.
     *
     * @param chainId
     * @param publicKey Returns the ACL the public key is assigned to. Can be used by the chain owner only. Key format in DER (ASN.1). (optional)
     * @param showAll Returns all ACLs of the chainId. Can be used by the chain owner only. (optional, default to false)
     * @return AclResponse
     */
    fun getAcl(
            chainId: UUID,
            publicKey: PublicKey? = null,
            showAll: Boolean? = null,
    ): Optional<AclResponse> {
        return aclService.getAcl(chainId, publicKey, showAll)
    }

//    /**
//     * Returns by default the ACL of the session owner for the given chain asynchronously.
//     * Parameters showAll and publicKey cannot be used together.
//     *
//     * @param chainId
//     * @param publicKey Returns the ACL the public key is assigned to. Can be used by the chain owner only. Key format in DER (ASN.1). (optional)
//     * @param showAll Returns all ACLs of the chainId. Can be used by the chain owner only. (optional, default to false)
//     */
//    suspend fun getAclAsync(
//            chainId: UUID,
//            publicKey: PublicKey? = null,
//            showAll: Boolean? = null,
//    ): Optional<AclResponse> = coroutineScope {
//        val deferred = async {
//            aclService.getAcl(chainId, publicKey, showAll)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }


    /**
     *  Creates a new session with a specific X-API-SessionToken that is needed
     *  for the GET and DELETE operations.
     */
    val session: UUID?
        get(): UUID? {
            return sessionService.session
        }


    /**
     *  Reads CCM's crypt id
     *
     *  @return ServerPublicKeySignature
     */
    val cryptIdServer: ServerPublicKeySignature
        get() {
            return sessionService.cryptIdServer
        }

//    /**
//     *  Reads CCM's crypt id asynchronously
//     *
//     *  @return a Deferred object, the contained value has to be get with await()
//     */
//    suspend fun getServerCryptIdAsync(): Optional<ServerPublicKeySignature> = coroutineScope {
//        val deferred = async {
//            Optional.of(sessionService.cryptIdServer)
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }

    /**
     * Terminates session before the expiration.
     */
    fun terminateSession() {
        sessionService.terminateSession()
    }

//    /**
//     * Terminates a session asynchronously before the expiration.
//     */
//    suspend fun terminateSessionAsync() = coroutineScope {
//        val deferred = async {
//            sessionService.terminateSession()
//        }
//        deferred.await()
//        deferred.getCompleted()
//    }
}