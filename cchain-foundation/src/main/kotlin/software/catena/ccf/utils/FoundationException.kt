package software.catena.ccf.utils

/**
 * Exception with message
 */
class FoundationException(message: String) : Exception(message)