package software.catena.ccf.utils

import java.text.SimpleDateFormat
import java.util.*

class Logger private constructor(private var VERBOSITY_MODE: VerbosityMode) {

    enum class VerbosityMode(val value: kotlin.Int) {
        NO(0), // no verbosity; suitable for FAST or VERY_FAST mode
        LOGGER(1),// default, logger is on
        LOGGER_AND_EXCEPTIONS(2)// logger is on, on error it throws an exception; suitable for debugging
    }

    private enum class LogType {
        INFO, WARNING, ERROR
    }

    companion object {
        @JvmStatic
        fun getInstance(verbosity: VerbosityMode = VerbosityMode.LOGGER): Logger? {
            return when (verbosity) {
                VerbosityMode.NO -> null
                else -> Logger(verbosity)
            }
        }
    }

    private val DATE_FORMAT: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    private fun log(logType: LogType, message: String) =
        println("[${DATE_FORMAT.format(Date())}] [$logType] $message")

    /**
     * Logs an information message
     * @param message Message
     */
    fun info(message: String) {
        log(LogType.INFO, message)
    }

    /**
     * Logs a warning message
     * @param message Message
     */
    fun warning(message: String) {
        log(LogType.WARNING, message)
    }

    /**
     * Logs an error message or throws an exception
     * @param message Message
     */
    @Throws(FoundationException::class)
    fun error(e: Exception, message: String?) {
        when (VERBOSITY_MODE) {
            VerbosityMode.LOGGER_AND_EXCEPTIONS -> {
                e.printStackTrace()
                log(LogType.ERROR, message ?: e.message ?: "")
                throw FoundationException(message ?: e.message ?: "")
            }
            VerbosityMode.LOGGER -> {
                log(LogType.ERROR, message ?: e.message ?: "")
            }
            VerbosityMode.NO -> {
            }
        }
    }
}