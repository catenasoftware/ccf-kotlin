package software.catena.ccf.models.chain

import software.catena.ccf.models.Notes

data class SingleChainResponse(
    val response: ChainsResponse,
) {
    val chain: ChainInfo
        get() = response.data[0]

    val note: Notes?
        get() = response.note
}