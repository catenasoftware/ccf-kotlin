package software.catena.ccf.models

import software.catena.ccf.models.acl.AclUser
import software.catena.ccf.models.transaction.TransactionRequestDataToHash
import java.lang.reflect.Modifier
import java.text.Collator
import java.time.OffsetDateTime
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import java.util.function.Consumer

object Reflection {

    /**
     * @param obj
     * @return
     * @throws IllegalAccessException
     * @throws ClassNotFoundException
     */
    @Throws(IllegalAccessException::class, ClassNotFoundException::class)
    fun getMembers(obj: Any): LinkedList<Pair> {
        val clazz: Class<*> = obj.javaClass
        val result = LinkedList<Pair>()
        val fields = clazz.declaredFields

        // go over all fields
        for (field in fields) {

            // allow access to private fields
            if (Modifier.isPrivate(field.modifiers)) {
                field.isAccessible = true
            }

            when {

                // handle null values
                field[obj] == null -> result.add(Pair(field.name, "null"))

                // handle UUID
                field.type == UUID::class.java -> result.add(
                    Pair(
                        field.name,
                        field[obj].toString()
                    )
                )

                // handle byte[]
                field.type == ByteArray::class.java -> result.add(
                    Pair(
                        field.name,
                        Base64.getEncoder().encodeToString(field[obj] as ByteArray)
                    )
                )

                //handle primitive values
                field.type.isPrimitive || field.type == String::class.java || field.type == Long::class.java
                        || field.type == Int::class.java || field.type == Float::class.java -> result.add(
                    Pair(field.name, field[obj].toString())
                )

                // handle OffsetDateTime
                field.type == OffsetDateTime::class.java -> result.add(
                    Pair(
                        field.name,
                        (field[obj] as OffsetDateTime).toString()
                    )
                )

                // handle Lists
                field.type == MutableList::class.java -> {
                    if (field.type == MutableList::class.java) {
                        if (MutableCollection::class.java.isAssignableFrom(field.type)) {

                            // get class name of field
                            val s = field.toGenericString()
                            val type =
                                s.split("\\<").toTypedArray()[1].split("\\>")
                                    .toTypedArray()[0]
                            // create class out of determined field type
                            val clazzInner = Class.forName(type)

                            // add values of AclUser
                            if (type == AclUser::class.java.name) {
                                val innerList =
                                    field[obj] as List<AclUser>
                                val ordinal = AtomicInteger(0)
                                innerList.forEach(Consumer { item: AclUser ->
                                    val iterationResult: LinkedList<Pair>
                                    try {
                                        iterationResult = getMembers(item)
                                        iterationResult.forEach(Consumer { itemB: Pair ->
                                            itemB.key = itemB.key + "_" + ordinal.get()
                                        })
                                        ordinal.incrementAndGet()
                                        result.addAll(iterationResult)
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                })
                            }
                        }

                    }
                }

                // handle Boolean
                field.type == Boolean::class.java -> result.add(
                    Pair(
                        field.name,
                        field[obj].toString()
                    )
                )

                // ignore generic class
                field.type == Class::class.java -> {
                }

                // recursion to get all fields of other classes
                field.type == TransactionRequestDataToHash.Algorithm::class.java -> result.add(
                    Pair(
                        field.name,
                        field[obj].toString()
                    )
                )
                else -> result.addAll(getMembers(field[obj]))

            }
        }


        // sort list ASC by key
        result.sortWith(Comparator { p1: Pair, p2: Pair ->
            Collator.getInstance().compare(p1.key, p2.key)
        })
        return result
    }

    @Throws(ClassNotFoundException::class, IllegalAccessException::class)
    fun getMembersValueAsConcatenatedString(o: Any): String {
        val pairList = getMembers(o)
        val stringList = LinkedList<String>()
        pairList.forEach(Consumer { item: Pair ->
            stringList.add(
                item.value
            )
        })
        val joined = java.lang.String.join(" \\\\ ", stringList)
        return joined
    }

}

class Pair(var key: String, var value: String) {
    override fun toString(): String {
        return "$key: $value"
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val pair = o as Pair
        return key == pair.key &&
                value == pair.value
    }

    override fun hashCode(): Int {
        return Objects.hash(key, value)
    }

}