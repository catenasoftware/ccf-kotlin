package software.catena.ccf.models.transaction

import software.catena.ccf.models.Notes
import software.catena.ccf.models.chain.ChainInfoTx

/**
 *
 * @param response
 */
data class SingleTransactionResponse(val transaction: Transaction,
                                     val chainInfo: ChainInfoTx,
                                     val note: Notes?,
                                     val payload: ByteArray) {
    
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SingleTransactionResponse

        if (transaction != other.transaction) return false
        if (chainInfo != other.chainInfo) return false
        if (note != other.note) return false
        if (!payload.contentEquals(other.payload)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = transaction.hashCode()
        result = 31 * result + chainInfo.hashCode()
        result = 31 * result + (note?.hashCode() ?: 0)
        result = 31 * result + payload.contentHashCode()
        return result
    }
}
