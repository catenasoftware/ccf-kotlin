package software.catena.ccf.models.session

/**
 *
 * @param publicKeyServer Format in DER (ASN.1).
 * @param signedPublicKeyServer ecdsa_server(publicKey)
 */
data class ServerPublicKeySignature(
    val publicKey: ByteArray,
    val signaturePublicKey: ByteArray,

    )