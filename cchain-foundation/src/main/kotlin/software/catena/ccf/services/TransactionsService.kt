package software.catena.ccf.services

import software.catena.ccf.crypto.CryptId
import software.catena.ccf.infrastructure.ApiRoute
import software.catena.ccf.infrastructure.RequestConfig
import software.catena.ccf.infrastructure.ServiceConfiguration
import software.catena.ccf.models.transaction.*
import software.catena.ccf.utils.FoundationException
import java.security.PublicKey
import java.util.*

open class TransactionsService(serviceConfiguration: ServiceConfiguration, private val sessionService: SessionService) : AbstractService(serviceConfiguration) {

    /**
     * Creates a new transaction in the given chain.
     *
     * @param chainId
     * @param cryptIdRecipient
     * @param typeId typeId 2 and 3 is reserved for ACL requests, 4-29 is reserved for future system use, 30 is a default transaction, >30 can be used by clients to distinguish between transaction types.
     * @param plain
     * @param payload
     * @param noReply Don't send signed transaction back. Only HTTP 201 (optional, default to false)
     *
     * @return TransactionResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun createTransaction(
            chainId: UUID,
            cryptIdRecipient: CryptId,
            payload: ByteArray,
            plain: Boolean = false,
            typeId: Int = Transaction.TransactionType.DEFAULT.value,
            noReply: Boolean = false,
    ): Optional<SingleTransactionResponse> {

        val dataToHash: TransactionRequestDataToHash = createTransactionRequest(
                chainId = chainId,
                transactionId = UUID.randomUUID(),
                publicKeyRecipient = cryptIdRecipient.publicKey,
                signedPublicKeyRecipient = cryptIdRecipient.publicKeySigned,
                payload = payload,
                plain = plain,
                typeId = typeId
        )

        val transactionRequest = TransactionRequest(dataToHash, serviceConfiguration.cryptoModule.signObjectMemberValues(dataToHash))

        val localVariableConfig = RequestConfig(
                method = RequestConfig.RequestMethod.POST,
                route = ApiRoute().chains().chainId(chainId).transactions().transactionId(dataToHash.transactionId))
                .addQueryParameter("noReply", noReply)

        val transactionResponse: Optional<TransactionResponse> = request(localVariableConfig, transactionRequest)
        if (transactionResponse.isEmpty) {
            return Optional.empty()
        }

        val response = createSingleTransactionResponse(transactionResponse.get(), serviceConfiguration)

        if (!noReply && transactionResponse.get().data.size != 1) {
                throw FoundationException("Transaction creation: Response has to have exactly one element")
        }

        return Optional.of(response)
    }

    /**
     * @param typeId is normally between 0 and 2^31-1, 30 is the default typeID for all common transactions.
     * 0 to 29 are reserved for special transactions, such as 0 for chain creation and 1 for chain info
     * modification and 2-3 for ACL configurations, then in such cases this method has to be overridden.
     *
     * @return true if the typeId is between 30 and 2147483646
     */
    override fun checkTypeIdRange(typeId: Int): Boolean {
        if (typeId < Transaction.TransactionType.DEFAULT.value || typeId > 2147483646) {
            throw FoundationException("Type Id has to be between 30 and 2147483646")
        }
        return true
    }

    /**
     * returns specific transaction
     * Returns a specific transaction given the chainId and transactionId.
     * @param chainId
     * @param transactionId
     * @return SingleTransactionResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun getTransactionById(
            chainId: UUID,
            transactionId: UUID,
    ): Optional<SingleTransactionResponse> {


        val localVariableConfig = RequestConfig(
                method = RequestConfig.RequestMethod.GET,
                route = ApiRoute().chains().chainId(chainId).transactions().transactionId(transactionId))
                .addHeaderXApiSession(sessionService.session!!)

        val transactionResponse: Optional<TransactionResponse> = request(localVariableConfig)

        if (transactionResponse.isEmpty) {
            return Optional.empty()
        }

        if (transactionResponse.get().data.size != 1)
            throw FoundationException("Get transaction by Id: Response has to have exactly one element")

        if (transactionResponse.get().data[0].transactions.size != 1)
            throw FoundationException("Get transaction by Id: Response has to have exactly one element")

        val response = createSingleTransactionResponse(transactionResponse.get(), serviceConfiguration)

        return Optional.of(response)
    }

    /**
     * Gets a filtered list of transactions.
     * Returns by default all transactions of all chains.
     *
     * @param chainId  (optional)
     * @param senderPublicKey Returns transactions matching the given senders public key. (optional)
     * @param recipientPublicKey Returns transactions matching the given recipient public key. (optional)
     * @param typeId Returns transactions matching the given typeId. (optional)
     * @param fromDate Returns transactions created after the given date-time. (optional)
     * @param untilDate Returns transactions created before the given date-time. (optional)
     * @param first [NOT IMPLEMENTED!] Returns the i first transactions. (optional)
     * @param untilId Returns all transactions until the given UUID. (optional)
     * @param fromId Returns all transactions after the given UUID. (optional)
     * @param last [NOT IMPLEMENTED!] Returns the last i transactions. (optional)
     * @return TransactionResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun getTransactions(
            chainId: UUID? = null,
            senderPublicKey: PublicKey? = null,
            recipientPublicKey: PublicKey? = null,
            typeId: Int? = null,
            fromDate: java.time.OffsetDateTime? = null,
            untilDate: java.time.OffsetDateTime? = null,
            fromId: UUID? = null,
            untilId: UUID? = null,
            first: Long? = null,
            last: Long? = null,
    ): Optional<TransactionResponse> {

        val localVariableConfig = RequestConfig(
                method = RequestConfig.RequestMethod.GET,
                route = ApiRoute().chains().transactions())
                .addHeaderXApiSession(sessionService.session!!)
                .addQueryParameter("chainId", chainId)
                .addQueryParameter("senderPublicKey", senderPublicKey)
                .addQueryParameter("recipientPublicKey", recipientPublicKey)
                .addQueryParameter("typeId", typeId)
                .addQueryParameter("fromDate", fromDate)
                .addQueryParameter("untilDate", untilDate)
                .addQueryParameter("fromId", fromId)
                .addQueryParameter("untilId", untilId)
                .addQueryParameter("first", first)
                .addQueryParameter("last", last)

        val transactionResponse: Optional<TransactionResponse> = request(localVariableConfig)
        if (transactionResponse.isEmpty) {
            return Optional.empty()
        }
        return transactionResponse
    }
}


