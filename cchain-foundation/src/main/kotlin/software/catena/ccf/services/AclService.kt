package software.catena.ccf.services

import software.catena.ccf.infrastructure.ApiRoute
import software.catena.ccf.infrastructure.RequestConfig
import software.catena.ccf.infrastructure.ServiceConfiguration
import software.catena.ccf.json.CCFJsonSerializer
import software.catena.ccf.models.acl.AclRequest
import software.catena.ccf.models.acl.AclResponse
import software.catena.ccf.models.transaction.*
import software.catena.ccf.utils.FoundationException
import java.security.PublicKey
import java.util.*

class AclService(serviceConfiguration: ServiceConfiguration, private val sessionService: SessionService) : AbstractService(serviceConfiguration) {

    /**
     * Updates an existing ACL for the given chain. New entries are added
     * incrementally, or when already existing, updated or deleted.
     *
     * @param chainId the chain this ACL belongs to
     * @param aclList The payload must contain a list of AclRequest elements.
     * @return TransactionResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun updateAcl(chainId: UUID, aclList: ArrayList<AclRequest> = arrayListOf()): Optional<SingleTransactionResponse> {
            return putAcl(chainId, aclList, Transaction.TransactionType.UPDATE_ACL.value)
    }

    /**
     * Updates an existing ACL for the given chain. A new entry is added,
     * or when already existing, updated or deleted.
     *
     * @param chainId the chain this ACL belongs to
     * @param publicKey PublicKey of the user the ACL is assigned to. Format in DER (ASN.1).
     * @param read If null publicRead is used otherwise value overrides publicRead value.
     * @param write If null publicWrite is used otherwise value overrides publicWrite value.
     * @param deleteAcl Delete ACL entry for given publicKey. Obsoletes other parameters.
     * @return TransactionResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun updateAcl(
        chainId: UUID,
        publicKey: ByteArray,
        read: Boolean? = null,
        write: Boolean? = null,
        deleteAcl: Boolean? = null,
    ): Optional<SingleTransactionResponse> {

        val aclRequest = AclRequest(publicKey, read, write, deleteAcl)
        val aclList: ArrayList<AclRequest> = arrayListOf(aclRequest)

        return updateAcl(chainId, aclList)
    }

    /**
     * Creates a new ACL for the given chain. The old ACL for the whole chain is deleted.
     * New entries are added incrementally if a list of AclRequest is specified.
     *
     * @param chainId the chain this ACL belongs to
     * @param aclList The payload could contain a list of AclRequest elements.
     * @return TransactionResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun replaceAcl(chainId: UUID, aclList: ArrayList<AclRequest> = arrayListOf()): Optional<SingleTransactionResponse> {
            return putAcl(chainId, aclList, Transaction.TransactionType.REPLACE_ACL.value)
    }

    /**
     * Creates a new ACL for the given chain. The old ACL for the whole chain is deleted.
     * A new entry is added, or when already existing, updated or deleted.
     *
     * @param chainId the chain this ACL belongs to
     * @param publicKey PublicKey of the user the ACL is assigned to. Format in DER (ASN.1).
     * @param read If null publicRead is used otherwise value overrides publicRead value.
     * @param write If null publicWrite is used otherwise value overrides publicWrite value.
     * @return TransactionResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun replaceAcl(
        chainId: UUID,
        publicKey: ByteArray,
        read: Boolean? = null,
        write: Boolean? = null,
    ): Optional<SingleTransactionResponse> {

        val aclRequest = AclRequest(publicKey, read, write, false)
        val aclList: ArrayList<AclRequest> = arrayListOf(aclRequest)

        return replaceAcl(chainId, aclList)
    }

    /**
     * Returns by default the ACL of the session owner for the given chain.
     * Parameters showAll and publicKey cannot be used together.
     *
     * If not received already in a ByteArray form, the public key parameter can be read in from a PublicKey object
     * via publicKey?.encoded, for future uses this Byte Array value should be cashed because this call costs
     * computation power, important when such calls are frequent
     *
     * @param chainId
     * @param publicKey Returns the ACL the public key is assigned to. Can be used by the chain owner only. Key format in DER (ASN.1). (optional)
     * @param showAll Returns all ACLs of the chainId. Can be used by the chain owner only. (optional, default to false)
     * @return AclResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun getAcl(
        chainId: UUID,
        publicKey: PublicKey? = null,
        showAll: Boolean? = null,
    ): Optional<AclResponse> {
        if (publicKey != null && showAll != null){
            throw FoundationException("Parameters showAll and publicKey cannot be used together!")
        }

        val localVariableConfig = RequestConfig(
                method = RequestConfig.RequestMethod.GET,
                route = ApiRoute().chains().chainId(chainId).acl())
                .addHeaderXApiSession(sessionService.session!!)
                .addQueryParameter("publicKey", publicKey)
                .addQueryParameter("showAll", showAll)

        return request(localVariableConfig)
    }


    /**
     * ACL configurations are special transactions and the typeId is 2 or 3
     */
    override fun checkTypeIdRange(typeId: Int): Boolean {
        if (typeId < Transaction.TransactionType.REPLACE_ACL.value || typeId > Transaction.TransactionType.UPDATE_ACL.value){
            throw FoundationException("Type Id has to be for ACL 2 or 3")
        }
        return true
    }

    private fun putAcl(chainId: UUID, aclList: ArrayList<AclRequest>, transactionType: Int): Optional<SingleTransactionResponse>{

        val plain = true// TODO in CCM-Go: TransactionRequestDataToHash.Algorithm.RSA_WITH_AES_128_CBC
        val dataToHash = createTransactionRequest(
                chainId = chainId,
                transactionId = UUID.randomUUID(),
                publicKeyRecipient =  sessionService.getServerPublicKey(),
                signedPublicKeyRecipient = sessionService.getServerSignature(),
                payload = CCFJsonSerializer.toJsonBytesUtf8(aclList),
                plain = plain,
                typeId = transactionType)

        val transactionRequest = TransactionRequest(dataToHash, serviceConfiguration.cryptoModule.signObjectMemberValues(dataToHash))

        val localVariableConfig = RequestConfig(
                method = RequestConfig.RequestMethod.PUT,
                route = ApiRoute().chains().chainId(chainId).acl())

        val aclResponse: Optional<TransactionResponse> = request(localVariableConfig, transactionRequest)

        if(aclResponse.isEmpty){
            return Optional.empty()
        }
        return Optional.of(createSingleTransactionResponse(aclResponse.get(),serviceConfiguration))
    }

}
