package software.catena.ccf.services

import com.google.common.base.CharMatcher
import software.catena.ccf.CCFService
import software.catena.ccf.infrastructure.ApiRoute
import software.catena.ccf.infrastructure.RequestConfig
import software.catena.ccf.infrastructure.ServiceConfiguration
import software.catena.ccf.models.chain.*
import software.catena.ccf.models.transaction.Transaction
import software.catena.ccf.utils.FoundationException
import java.security.PublicKey
import java.time.OffsetDateTime
import java.util.*

class ChainsService(serviceConfiguration: ServiceConfiguration, private val sessionService: SessionService) : AbstractService(serviceConfiguration) {


    /**
     * @param name Format UTF-8 can contain all characters (Regex \\P{Cc}) except controll characters. Maximum 255 bytes long.
     * @param description Format UTF-8 can contain all characters (Regex \\P{Cc}) except controll characters. Maximum 255 bytes long.
     * @param closed If closed no one can write into chain. The chain owner only can change the state.
     * @param publicRead
     * @param publicWrite
     */
    fun createChain(
        /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except controll characters. Maximum 255 bytes long. */
        name: String,
        /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except controll characters. Maximum 255 bytes long. */
        description: String,
        /* If closed no one can write into chain. The chain owner only can change the state. */
        closed: Boolean = false,
        publicRead: Boolean = true,
        publicWrite: Boolean = true,

        ): Optional<SingleChainResponse> {

//        TODO PWE: input validation
//            if (serviceConfiguration.securityMode < CCFService.SecurityMode.VERY_FAST) {// no plausibility checks in very fast mode
//                val cleanedName: String = CharMatcher.javaIsoControl().removeFrom(name)
//                val cleanedDescription: String =
//                    CharMatcher.javaIsoControl().removeFrom(description)
//                if (name != cleanedName) {
//                    throw Exception("Name contains control characters!")
//                }
//                if (description != cleanedDescription) {
//                    throw Exception("Description contains control characters!")
//                }
//            }


            val chainInfoTxRequestDataToHash = ChainInfoTxRequestDataToHash(
                transactionId = UUID.randomUUID(),
                chainId = UUID.randomUUID(),
                name = name,
                description = description,
                closed = closed,
                publicKeyOwner = serviceConfiguration.keyPair.publicKeyEncoded,
                signedPublicKeyOwner = serviceConfiguration.keyPair.cryptId.publicKeySigned,
                typeId = Transaction.TransactionType.CREATE_CHAIN.value,
                publicRead = publicRead,
                publicWrite = publicWrite)

            val chainInfoTxRequest: ChainInfoTxRequest = createChainInfoTxRequest(chainInfoTxRequestDataToHash)

            val localVariableConfig = RequestConfig(method = RequestConfig.RequestMethod.POST,
                                                    route = ApiRoute().chains().chainId(chainInfoTxRequestDataToHash.chainId))

            val chainsResponse: Optional<ChainsResponse> =  request(localVariableConfig, chainInfoTxRequest)

            if(chainsResponse.isEmpty){
                return Optional.empty()
            }

            if (chainsResponse.get().data.size != 1){
                throw FoundationException("Chain creation: Response has to have exactly one element")
            }

            val response = SingleChainResponse(chainsResponse.get())
            return Optional.of(response)
    }

    /**
     * @param chain the chain to be updated
     * @param name Format UTF-8 can contain all characters (Regex \\P{Cc}) except controll characters. Maximum 255 bytes long.
     * @param description Format UTF-8 can contain all characters (Regex \\P{Cc}) except controll characters. Maximum 255 bytes long.
     * @param closed If closed no one can write into chain. The chain owner only can change the state.
     * @param publicRead
     * @param publicWrite
     *
     * @return a new object with updated values
     */
    fun updateChain(
        chain: ChainInfo,
        /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except controll characters. Maximum 255 bytes long. */
        name: String? = null,
        /* Format UTF-8 can contain all characters (Regex \\P{Cc}) except controll characters. Maximum 255 bytes long. */
        description: String? = null,
        /* If closed no one can write into chain. The chain owner only can change the state. */
        closed: Boolean? = null,
        publicRead: Boolean? = null,
        publicWrite: Boolean? = null,
    ): Optional<SingleChainResponse> {

//        TODO PWE: validation
//            if (serviceConfiguration.securityMode < CCFService.SecurityMode.VERY_FAST) {// no plausibility checks in very fast mode
//                if (name != null) {
//                    val cleanedName: String = CharMatcher.javaIsoControl().removeFrom(name)
//                    if (name != cleanedName) {
//                        throw Exception("Name contains control characters!")
//                    }
//                }
//
//                if (description != null) {
//                    val cleanedDescription: String =
//                        CharMatcher.javaIsoControl().removeFrom(description)
//                    if (description != cleanedDescription) {
//                        throw Exception("Description contains control characters!")
//                    }
//                }
//
//                if (name == null && description == null &&
//                    closed == null && publicRead == null && publicWrite == null
//                )
//                    throw FoundationException("No changes in chain")
//            }

            val chainInfoTxRequestDataToHash = ChainInfoTxRequestDataToHash(
                transactionId = UUID.randomUUID(),
                chainId = chain.chainId,
                name = name ?: chain.name,
                description= description ?: chain.description,
                closed= closed ?: chain.closed,
                publicKeyOwner = serviceConfiguration.keyPair.publicKeyEncoded,
                signedPublicKeyOwner = serviceConfiguration.keyPair.cryptId.publicKeySigned,
                typeId = Transaction.TransactionType.UPDATE_CHAIN.value,
                publicRead = publicRead ?: chain.publicRead,
                publicWrite= publicWrite ?: chain.publicWrite
            )

            val chainInfoTxRequest = createChainInfoTxRequest(chainInfoTxRequestDataToHash)
            val localVariableConfig = RequestConfig(method = RequestConfig.RequestMethod.PUT,
                                                    route = ApiRoute().chains().chainId(chainInfoTxRequestDataToHash.chainId))

            val chainsResponse: Optional<ChainsResponse> = request(localVariableConfig, chainInfoTxRequest)

            return if (chainsResponse.isPresent){
              Optional.of(SingleChainResponse(chainsResponse.get()))
            } else{
                Optional.empty()
            }
    }


    /**
     * get specific chain
     * Returns the details of a specific chain by chainId.
     * @param chainId
     * @return SingleChainResponse
     */
    fun getChainById(chainId: UUID): Optional<SingleChainResponse> {

        val localVariableConfig = RequestConfig(
                method = RequestConfig.RequestMethod.GET,
                route = ApiRoute().chains().chainId(chainId))
                .addHeaderXApiSession(sessionService.session!!)

        val chainsResponse: Optional<ChainsResponse> =  request(localVariableConfig)

        if(chainsResponse.isEmpty){
            return Optional.empty()
        }

        if (chainsResponse.get().data.size != 1){
            throw FoundationException("Get chain by Id: Response has to have exactly one element")
        }

        val response = SingleChainResponse(chainsResponse.get())
        return Optional.of(response)

    }

    /**
     * returns array of chains
     * Returns by default all chains managed by the CCM.
     *
     *  If not received already in a ByteArray form, the public key parameters can be read in from a PublicKey object
     *  via publicKey?.encoded, for future uses this Byte Array value should be cashed because this call costs
     *  computation power, important when such calls are frequent
     *
     * @param ownerPublicKey Returns chains the given public keys is the chain owner. (optional)
     * @param nameContains Returns chains where the name contains the given string. (optional)
     * @param descriptionContains Returns chains where the description contains the given string. (optional)
     * @param publicRead Returns chains which are either readable by everyone or not. (optional)
     * @param publicWrite Returns chains which are either writeable by everyone or not. (optional)
     * @param closed Returns chains which are either open or closed. (optional)
     * @param fromDate returns items created after the given date-time. (optional)
     * @param untilDate returns items created before the given date-time. (optional)
     * @param fromId Returns all items after the given UUID. (optional)
     * @param untilId Returns all items until the given UUID. (optional)
     * @param first [NOT IMPLEMENTED!] returns the first n items. (optional)
     * @param last [NOT IMPLEMENTED!] returns the last n items. (optional)
     * @param transactionSenderPublicKey [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given sender public key. (optional)
     * @param transactionRecipientPublicKey [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given recipient public key. (optional)
     * @param transactionFromDate [NOT IMPLEMENTED!] Returns all chains containing transactions after the given date-time. (optional)
     * @param transactionUntilDate [NOT IMPLEMENTED!] Returns all chains containing transactions before the given date-time. (optional)
     * @param transactionTypeId [NOT IMPLEMENTED!] Returns all chains containing transactions matching the given typeId. (optional)
     * @param transactionMaximumCount [NOT IMPLEMENTED!] Returns all chains containing maximum n transactions. (optional)
     * @param transactionMinimumCount [NOT IMPLEMENTED!] Returns all chains containing minimum n transactions. (optional)
     * @return ChainsResponse
     */
    @Suppress("UNCHECKED_CAST")
    fun getChains(
        ownerPublicKey: PublicKey? = null,
        nameContains: String? = null,
        descriptionContains: String? = null,
        publicRead: Boolean? = null,
        publicWrite: Boolean? = null,
        closed: Boolean? = null,
        fromDate: OffsetDateTime? = null,
        untilDate: OffsetDateTime? = null,
        fromId: UUID? = null,
        untilId: UUID? = null,
        first: Long? = null,
        last: Long? = null,
        transactionSenderPublicKey: PublicKey? = null,
        transactionRecipientPublicKey: PublicKey? = null,
        transactionFromDate: OffsetDateTime? = null,
        transactionUntilDate: OffsetDateTime? = null,
        transactionTypeId: Int? = null,
        transactionMaximumCount: Long? = null,
        transactionMinimumCount: Long? = null,
    ): Optional<ChainsResponse> {

        val localVariableConfig = RequestConfig(
                method = RequestConfig.RequestMethod.GET,
                route = ApiRoute().chains())
                .addHeaderXApiSession(sessionService.session!!)
                .addQueryParameter("ownerPublicKey", ownerPublicKey)
                .addQueryParameter("nameContains", nameContains)
                .addQueryParameter("descriptionContains", descriptionContains)
                .addQueryParameter("publicRead", publicRead)
                .addQueryParameter("publicWrite", publicWrite)
                .addQueryParameter("closed", closed)
                .addQueryParameter("fromDate", fromDate)
                .addQueryParameter("untilDate", untilDate)
                .addQueryParameter("fromId", fromId)
                .addQueryParameter("untilId", untilId)
                .addQueryParameter("first", first)
                .addQueryParameter("last", last)
                .addQueryParameter("transactionSenderPublicKey", transactionSenderPublicKey)
                .addQueryParameter("transactionRecipientPublicKey", transactionRecipientPublicKey)
                .addQueryParameter("transactionFromDate", transactionFromDate)
                .addQueryParameter("transactionUntilDate", transactionUntilDate)
                .addQueryParameter("transactionTypeId", transactionTypeId)
                .addQueryParameter("transactionMaximumCount", transactionMaximumCount)
                .addQueryParameter("transactionMinimumCount", transactionMinimumCount)

        return request(localVariableConfig)
    }

    private fun createChainInfoTxRequest(chainInfoTxRequestDataToHash: ChainInfoTxRequestDataToHash): ChainInfoTxRequest{
        return ChainInfoTxRequest(chainInfoTxRequestDataToHash, serviceConfiguration.cryptoModule.signObjectMemberValues(chainInfoTxRequestDataToHash))
    }

    override fun checkTypeIdRange(typeId: Int): Boolean {
        return true
    }
}