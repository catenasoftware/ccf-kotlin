package software.catena.ccf.services

import software.catena.ccf.CCFService
import software.catena.ccf.crypto.ECC
import software.catena.ccf.crypto.KeyHandling
import software.catena.ccf.crypto.RSA
import software.catena.ccf.infrastructure.ServiceConfiguration
import software.catena.ccf.models.Reflection
import software.catena.ccf.models.session.*
import software.catena.ccf.utils.FoundationException
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import software.catena.ccf.infrastructure.ApiRoute
import software.catena.ccf.infrastructure.RequestConfig
import java.security.PublicKey
import java.time.OffsetDateTime
import java.util.*

open class SessionService(serviceConfiguration: ServiceConfiguration): AbstractService(serviceConfiguration) {


        private var sessionToken: UUID? = null
        private var sessionExpiration: OffsetDateTime? = null

        var oid: ASN1ObjectIdentifier? = null

        private var publicKeyServer: PublicKey? = null
        private var publicKeyServerEncoded: ByteArray? = null
        private var signatureServer: ByteArray? = null

        private var _cryptIdServer: ServerPublicKeySignature? = null
        val cryptIdServer: ServerPublicKeySignature
            get() {
                if (_cryptIdServer != null)
                    return _cryptIdServer!!

                _cryptIdServer = getServerPublicKeySignature().get()

                checkServerCryptId()

                return _cryptIdServer!!
            }

    private fun getServerPublicKeySignature(): Optional<ServerPublicKeySignature> {
        val localVariableConfig = RequestConfig(method = RequestConfig.RequestMethod.GET, route = ApiRoute().root())
        return request(localVariableConfig)
    }


        private fun checkServerCryptId(): Boolean {

            // check cryptID recipient
//            if (serviceConfiguration.securityMode < CCFService.SecurityMode.FAST) {
//                if (!KeyHandling.checkCryptId(_cryptIdServer!!.publicKey,
//                        _cryptIdServer!!.signaturePublicKey)
//                ) {
//                    throw FoundationException("Error when reading CCM server crypt id: Signature of recipient's public key is invalid!")
//                }
//            }

            if (publicKeyServerEncoded != null && publicKeyServer != null)
                assert(publicKeyServerEncoded!! contentEquals _cryptIdServer!!.publicKey)
            else {
                publicKeyServerEncoded = _cryptIdServer!!.publicKey
                publicKeyServer =
                    KeyHandling.getPublicKeyFromASN(publicKeyServerEncoded)
            }

            signatureServer = _cryptIdServer!!.signaturePublicKey

            val oidCCM: ASN1ObjectIdentifier =
                KeyHandling.getOID(publicKeyServerEncoded)

            if (oid == null)
                oid = KeyHandling.getOID(serviceConfiguration.keyPair.publicKeyEncoded)

            if (serviceConfiguration.securityMode < CCFService.SecurityMode.VERY_FAST) {
                if (!oidCCM.equals(oid)) {
                    throw FoundationException("Error when reading CCM server crypt id: Keys' oid don't match!")
                }
                checkOID(oidCCM)
            }

            return true
        }


        /**
         *  Reads CCM's public key
         */
        fun getServerPublicKey(): PublicKey {

            return if (publicKeyServer != null) {
                publicKeyServer!!
            } else {
                // byte[]
                publicKeyServerEncoded = cryptIdServer.publicKey
                // convert byte[] to PublicKey
                publicKeyServer =
                    KeyHandling.getPublicKeyFromASN(publicKeyServerEncoded)
                publicKeyServer!!
            }
        }

        /**
         *  Reads CCM's signature asynchronously
         */
        fun getServerSignature(): ByteArray {

            return if (signatureServer != null) {
                signatureServer!!
            } else {
                signatureServer = cryptIdServer.signaturePublicKey
                signatureServer!!
            }
        }

        /**
         *  Creates a new session with a specific X-API-SessionToken that is needed
         *  for the GET and DELETE operations.
         */
        val session: UUID?
            get(): UUID? {

                if (sessionToken != null && sessionExpiration != null && sessionExpiration!!.isAfter(
                        OffsetDateTime.now()
                    )
                ) {
                    return sessionToken
                }

                try {
                    //  Handshake: First a connect token is needed to request the session token
                    val useRSA: Boolean? = serviceConfiguration.keyPair.privateKey.algorithm == "RSA"
                    val sessionGetResponse: SessionGetResponse = getConnectToken(useRSA).get()
                    val connectToken = readConnectTokenServer(sessionGetResponse)
                    val sessionData = sendConnectTokenClient(connectToken)

                    sessionToken = sessionData!!.sessionToken
                    sessionExpiration = sessionData.sessionExpiration
                    serviceConfiguration.logger?.info(
                        "Session creation was successful (session = '$sessionToken', expirationDate = '$sessionExpiration')"
                    )
                    return sessionToken
                } catch (e: Exception) {
                    serviceConfiguration.logger?.error(
                        e,
                        "Error when creating session (message = '${e.message}')"
                    )

                    return null
                }
            }

    private fun getConnectToken(useRSA: Boolean? = null): Optional<SessionGetResponse> {

        val localVariableConfig: RequestConfig = if (useRSA != null) {
            RequestConfig(method = RequestConfig.RequestMethod.GET, route = ApiRoute().session().connecttoken())
                    .addQueryParameter("useRSA", useRSA)
        } else {
            RequestConfig(
                    method = RequestConfig.RequestMethod.GET,
                    route = ApiRoute().session().connecttoken()
            )
        }
        return request(localVariableConfig)
    }

        private fun readConnectTokenServer(sessionGetResponse: SessionGetResponse): UUID {

            val connectTokenServer: ConnectTokenServer = sessionGetResponse.data
            val connectToken: UUID = connectTokenServer.connectToken

            // byte[]
            publicKeyServerEncoded = connectTokenServer.publicKeyServer
            // convert byte[] to PublicKey
            publicKeyServer =
                KeyHandling.getPublicKeyFromASN(connectTokenServer.publicKeyServer)

            when (serviceConfiguration.keyPair.privateKey.algorithm) {
                "RSA" -> {
                    if (publicKeyServer!!.algorithm != "RSA") {
                        throw FoundationException("CCM's public key algorithm is not RSA!")
                    }
                }
                "EC" -> {
                    if (publicKeyServer!!.algorithm != "EC") {
                        throw FoundationException("CCM's public key algorithm is not ECC with curve secp256r1.")
                    }
                }
                else -> {
                    throw FoundationException("Unknown public key algorithm of CCM!")
                }
            }

            oid = KeyHandling.getOID(serviceConfiguration.keyPair.publicKeyEncoded)
            if (serviceConfiguration.securityMode < CCFService.SecurityMode.VERY_FAST) {
                checkOID(oid!!)

                val oidServer =
                    KeyHandling.getOID(publicKeyServerEncoded)

                if (!oidServer.equals(oid)) {
                    throw Exception("Keys don't match!")
                }
            }

            serviceConfiguration.logger?.info("Connect was successful (connectToken = '$connectToken')")

            return connectToken

        }

        private fun sendConnectTokenClient(connectToken: UUID): SessionDataToHash? {

            val tokenSignature: ByteArray? = signPlainText(connectToken.toString())

            val connectTokenClient = ConnectTokenClient(serviceConfiguration.keyPair.publicKeyEncoded, connectToken, tokenSignature!!)

            val sessionPostResponse: SessionPostResponse? = postSession(connectToken, connectTokenClient).get()

            val session: Session = sessionPostResponse!!.data

            validateSession(session)

            return session.dataToHash
        }

        private fun postSession(
                connectToken: UUID,
                body: ConnectTokenClient? = null,
        ): Optional<SessionPostResponse> {
            val localVariableBody: Any? = body

            val localVariableConfig = RequestConfig(
                    method = RequestConfig.RequestMethod.POST,
                    route = ApiRoute().session().sessionToken(connectToken)
            )
            return request(localVariableConfig, localVariableBody)
        }

        private fun validateSession(session: Session) {
            // validate response signature
            if (serviceConfiguration.securityMode < CCFService.SecurityMode.FAST) {
                // extract values as concatenated string for hash and signature computation
                val concatenatedString: String =
                    Reflection.getMembersValueAsConcatenatedString(session.dataToHash)


                if (!validatePlainText(
                        concatenatedString,
                        session.signedHashServer,
                        publicKeyServer!!
                    )
                ) {
                    throw FoundationException("Validation of signed response data failed!")
                }
            }
        }

        /**
         * Terminates session before the expiration.
         */
        fun terminateSession() {
            try {
                deleteSession(session!!)
                serviceConfiguration.logger?.info("Session successfully terminated.")
            } catch (e: Exception) {
                serviceConfiguration.logger?.error(
                    e,
                    "Error when terminating session (message = '${e.message}')"
                )
            }
        }

        private fun deleteSession(sessionToken: UUID) {

            val localVariableConfig = RequestConfig(
                    method = RequestConfig.RequestMethod.DELETE,
                    route = ApiRoute().session().sessionToken(sessionToken))
                    .addHeaderXApiSession(sessionToken)

            request<Any?>(localVariableConfig)
        }

        private fun validatePlainText(
                plainText: String,
                signatureASN1DER: ByteArray,
                publicKey: PublicKey,
        ): Boolean {
            return when (serviceConfiguration.keyPair.privateKey.algorithm) {
                "RSA" -> RSA.validatePlainText(plainText, signatureASN1DER, publicKey)
                "EC" -> ECC.validatePlainText(plainText, signatureASN1DER, publicKey)
                else -> throw FoundationException("Unknown algorithm!")
            }
        }

        fun signPlainText(plainText: String): ByteArray {
            return serviceConfiguration.cryptoModule.signPlainText(plainText)
        }

        private fun checkOID(oid: ASN1ObjectIdentifier): Boolean {
            return when (serviceConfiguration.keyPair.privateKey.algorithm) {
                "RSA" -> if (oid.id != "1.2.840.113549.1.1.1") {
                    throw FoundationException("RSA, OID: 1.2.840.113549.1.1.1 is supported only!")
                } else true
                "EC" -> if (oid.id != "1.2.840.10045.3.1.7") {
                    throw FoundationException("EC Curve secp256r1, OID: 1.2.840.10045.3.1.7 is supported only!")
                } else true
                else -> throw FoundationException("Unknown algorithm!")
            }
        }

    override fun checkTypeIdRange(typeId: Int): Boolean {
        return true
    }
}