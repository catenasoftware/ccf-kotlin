package software.catena.ccf.services

import software.catena.ccf.infrastructure.*
import software.catena.ccf.json.CCFJsonSerializer
import okhttp3.*
import software.catena.ccf.crypto.AES
import software.catena.ccf.crypto.EncryptionData
import software.catena.ccf.models.Payload
import software.catena.ccf.models.transaction.*
import java.security.PublicKey
import java.util.*
import javax.crypto.SecretKey


abstract class AbstractService(protected val serviceConfiguration: ServiceConfiguration) {

    private val contentType: String = "Content-Type"
    private val accept: String = "Accept"
    private val jsonMediaType: String = "application/json"
    protected val jsonHeaders: Map<String, String> = mapOf(contentType to jsonMediaType, accept to jsonMediaType)
    protected val httpUrl get() = serviceConfiguration.httpUrl

    protected val httpClient: OkHttpClient = OkHttpClientFactory.getOkHttpClient(httpUrl.toString(), serviceConfiguration.httpClientType)

    /**
     *  Takes any class and converts it to a HTTP RequestBody using JSON-Format
     */
    protected fun toRequestBody(content: Any, mediaType: String = jsonMediaType): RequestBody {
        return RequestBody.create(MediaType.parse(mediaType), CCFJsonSerializer.toJsonBytesUtf8(content))
    }

    /**
     *  Takes a HTTP ResponseBody and converts it to a kotlin class
     */
    protected inline fun <reified T> toResponseClass(body: ResponseBody?): T? {
        return CCFJsonSerializer.fromJson(T::class.java, body?.source())
    }

    /**
     * Make the real (sync) HTTP-Request here
     */
    protected inline fun <reified T : Any?> request(requestConfig: RequestConfig, body: Any? = null): Optional<T> {

        val urlBuilder: HttpUrl.Builder = HttpUrl.Builder()
                .scheme(httpUrl.scheme())
                .host(httpUrl.host())
                .port(httpUrl.port())
                .addPathSegments(requestConfig.route.getRoute().trimStart('/'))

        requestConfig.queryParam.forEach { query ->
            query.value.forEach { queryValue ->
                urlBuilder.addQueryParameter(query.key, queryValue)
            }
        }

        val url: HttpUrl = urlBuilder.build()
        val headers: Map<String, String> = requestConfig.headers + jsonHeaders

        val request: Request.Builder = when (requestConfig.method) {
            RequestConfig.RequestMethod.DELETE -> Request.Builder().url(url).delete()
            RequestConfig.RequestMethod.GET -> Request.Builder().url(url).get()
            RequestConfig.RequestMethod.PUT -> Request.Builder().url(url).put(toRequestBody(body!!))
            RequestConfig.RequestMethod.POST -> Request.Builder().url(url).post(toRequestBody(body!!))
        }

        headers.forEach { header -> request.addHeader(header.key, header.value) }

        httpClient.newCall(request.build()).execute().use { response: Response ->
            when {
                response.isSuccessful -> {
                    val deserializedBody: T? = toResponseClass(response.body())
                    return if (deserializedBody == null) {
                        Optional.empty()
                    } else {
                        return Optional.of(deserializedBody as T)
                    }
                }
                else -> return Optional.empty<T>()
            }
        }
    }

    protected fun createTransactionRequest(
            chainId: UUID,
            transactionId: UUID,
            publicKeyRecipient: PublicKey,
            signedPublicKeyRecipient: ByteArray,
            payload: ByteArray,
            plain: Boolean,
            typeId: Int,
    ): TransactionRequestDataToHash {

        val payloadObject: String = CCFJsonSerializer.toJson(
                Payload(serviceConfiguration.keyPair.publicKeyEncoded, payload))

        var payloadBytes: ByteArray = payloadObject.toByteArray(Charsets.UTF_8)

        /*
         * Prepare variables that might be set if plain is false
         */
        var symmetricKeySender: ByteArray? = null
        var symmetricKeyRecipient: ByteArray? = null
        var initialisationVector: ByteArray? = null
        var algorithm = TransactionRequestDataToHash.Algorithm.PLAIN

        /*
         * Encrypt the payload if plain is false
         */
        if (!plain) {

            algorithm = serviceConfiguration.cryptoModule.getCryptoAlgorithm()

            val payloadEncryptionData: EncryptionData = AES.encryptRSA(payloadBytes)
            initialisationVector = payloadEncryptionData.iv.iv
            payloadBytes = payloadEncryptionData.encryptedData

            assert(initialisationVector!!.size == 16)
            assert(payloadBytes.size >= 16)

            val symmetricKey: SecretKey = payloadEncryptionData.key
            symmetricKeySender = serviceConfiguration.cryptoModule.encryptSecretKey(symmetricKey)
            symmetricKeyRecipient = serviceConfiguration.cryptoModule.encryptSecretKey(symmetricKey, publicKeyRecipient)
        }

        // create the final TransactionRequestDataToHash
        return TransactionRequestDataToHash(
                transactionId = transactionId,
                publicKeySender = serviceConfiguration.keyPair.publicKeyEncoded,
                signedPublicKeySender = serviceConfiguration.keyPair.cryptId.publicKeySigned,
                publicKeyRecipient = publicKeyRecipient.encoded,
                signedPublicKeyRecipient = signedPublicKeyRecipient,
                chainId = chainId,
                typeId = typeId,
                symmetricKeySender = symmetricKeySender,
                symmetricKeyRecipient = symmetricKeyRecipient,
                initialisationVector = initialisationVector,
                algorithm = algorithm,
                payload = payloadBytes)
    }


    fun createSingleTransactionResponse(transactionResponse: TransactionResponse, serviceConfiguration: ServiceConfiguration): SingleTransactionResponse {

        assert(transactionResponse.data.size == 1)
        assert(transactionResponse.data.first().transactions.size == 1)


        val transactionContainer: TransactionContainer = transactionResponse.data.first().transactions.first()

        return SingleTransactionResponse(transactionContainer.transaction!!,
                transactionContainer.chainInfo!!,
                transactionResponse.note,
                decryptPayload(transactionContainer.transaction, serviceConfiguration))
    }

    private fun decryptPayload(transaction: Transaction, serviceConfiguration: ServiceConfiguration): ByteArray {
        /*
     * RSA: decrypt payload that was encrypted with a symmetric key that was encrypted with the rsa key of the sender
     */

        var payloadRaw = transaction.payloadRaw

        if (transaction.algorithm != TransactionRequestDataToHash.Algorithm.PLAIN) {
            val publicKeyCcf = serviceConfiguration.keyPair.publicKeyEncoded

            val symmetricKey = when {
                transaction.publicKeyRecipient contentEquals publicKeyCcf -> transaction.symmetricKeyRecipient
                transaction.publicKeySender contentEquals publicKeyCcf -> transaction.symmetricKeySender
                else -> return payloadRaw
            }

            val secretKeyDecrypted = serviceConfiguration.cryptoModule.decryptSecretKey(symmetricKey!!)
            // decrypt payload and deserialize it
            payloadRaw = AES.decryptRSA(payloadRaw, secretKeyDecrypted, transaction.initialisationVector!!)
        }

        val payloadObject: Payload = CCFJsonSerializer.fromJson(Payload::class.java, payloadRaw) as Payload

        transaction.payload = payloadObject.data
        return transaction.payload!!
    }


    protected abstract fun checkTypeIdRange(typeId: Int): Boolean
}