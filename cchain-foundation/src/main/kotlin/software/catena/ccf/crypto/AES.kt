package software.catena.ccf.crypto

import java.security.*
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object AES {
    private val random = SecureRandom()

    @Throws(
        InvalidKeyException::class,
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class
    )
    fun encryptECC(toEncrypt: ByteArray, ecdhSharedKey: ByteArray): EncryptionData {
        val key = generateKeyECC(ecdhSharedKey)
        return encrypt(toEncrypt, key)
    }

    @Throws(
        InvalidKeyException::class,
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class
    )
    fun encryptRSA(toEncrypt: ByteArray): EncryptionData {
        val key = generateKeyRSA(128)
        return encrypt(toEncrypt, key)
    }

    @Throws(
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class,
        InvalidKeyException::class,
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class
    )
    private fun encrypt(toEncrypt: ByteArray, key: SecretKey): EncryptionData {
        val cipher = SecurityProvider.getCipher("AES/CBC/PKCS7Padding")
        val iv = generateIV(cipher)
        cipher.init(Cipher.ENCRYPT_MODE, key, iv)
        val encrypted = cipher.doFinal(toEncrypt)
        return EncryptionData(encrypted, iv, key)
    }

    @Throws(
        InvalidKeyException::class,
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class
    )
    fun decryptRSA(toDecrypt: ByteArray, key: SecretKey, iv: ByteArray): ByteArray {
        return decrypt(toDecrypt, key, iv)
    }

    @Throws(
        InvalidKeyException::class,
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class
    )
    fun decryptECC(toDecrypt: ByteArray, ecdhSharedKey: ByteArray, iv: ByteArray): ByteArray {
        val key = generateKeyECC(ecdhSharedKey)
        return decrypt(toDecrypt, key, iv)
    }

    @Throws(
        InvalidKeyException::class,
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class
    )
    private fun decrypt(toDecrypt: ByteArray, key: SecretKey, iv: ByteArray): ByteArray {
        val cipher = SecurityProvider.getCipher("AES/CBC/PKCS7Padding")
        cipher.init(Cipher.DECRYPT_MODE, key, IvParameterSpec(iv))
        return cipher.doFinal(toDecrypt)
    }

    private fun generateIV(cipher: Cipher): IvParameterSpec {
        val ivBytes = ByteArray(cipher.blockSize)
        random.nextBytes(ivBytes)
        return IvParameterSpec(ivBytes)
    }

    private fun generateKeyRSA(keySize: Int): SecretKey {
        val keyBytes = ByteArray(keySize / 8)
        random.nextBytes(keyBytes)
        return SecretKeySpec(keyBytes, "AES")
    }

    private fun generateKeyECC(ecdhSharedKey: ByteArray): SecretKey {
        return SecretKeySpec(ecdhSharedKey, 0, 16, "AES")
    }
}