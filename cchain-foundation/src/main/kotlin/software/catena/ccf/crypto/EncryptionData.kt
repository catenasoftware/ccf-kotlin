package software.catena.ccf.crypto

import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec

class EncryptionData constructor(
    val encryptedData: ByteArray,
    val iv: IvParameterSpec,
    val key: SecretKey,
)