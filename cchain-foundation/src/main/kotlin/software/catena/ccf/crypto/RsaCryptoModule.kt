package software.catena.ccf.crypto

import software.catena.ccf.models.transaction.TransactionRequestDataToHash
import java.nio.charset.StandardCharsets
import java.security.KeyPair
import java.security.PrivateKey
import java.security.PublicKey
import java.security.spec.MGF1ParameterSpec
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.OAEPParameterSpec
import javax.crypto.spec.PSource
import javax.crypto.spec.SecretKeySpec

class RsaCryptoModule(keyPair: KeyPair) : AsymmetricCryptoModule(keyPair) {

    constructor(keyPair: RsaKeyPair) : this(keyPair.keyPair)

    override fun getCryptoAlgorithm(): TransactionRequestDataToHash.Algorithm {
        return TransactionRequestDataToHash.Algorithm.RSA_WITH_AES_128_CBC
    }

    override fun validatePlainBytes(plain: ByteArray, signature_ASN1_DER: ByteArray): Boolean {
        return validate(
                data = plain,
                signature_ASN1_DER = signature_ASN1_DER,
                algorithm = "SHA256withRSA/PSS") // SHA512withRSA/PSS, SHA256withRSA)
    }

    override fun validatePlainText(plainText: String, signature_ASN1_DER: ByteArray): Boolean {
        return validate(
                data = plainText.toByteArray(StandardCharsets.UTF_8),
                signature_ASN1_DER = signature_ASN1_DER,
                algorithm = "SHA256withRSA/PSS") // SHA512withRSA/PSS, SHA256withRSA


    }

    override fun validate(data: ByteArray, signature_ASN1_DER: ByteArray, algorithm: String, publicKey: PublicKey): Boolean {
        val publicSignature = SecurityProvider.getSignature(algorithm)
        publicSignature.initVerify(publicKey)
        publicSignature.update(data)
        return publicSignature.verify(signature_ASN1_DER)
    }

    override fun signPlainText(plainText: String): ByteArray {
        return sign(data = plainText.toByteArray(StandardCharsets.UTF_8), algorithm = "SHA256withRSA/PSS") // SHA512withRSA/PSS, SHA256withRSA

    }

    override fun signPlainBytes(plain: ByteArray): ByteArray {
        return sign(plain, "SHA256withRSA/PSS") // SHA512withRSA/PSS, SHA256withRSA
    }

    override fun sign(data: ByteArray, algorithm: String, privateKey: PrivateKey): ByteArray {
        val privateSignature = SecurityProvider.getSignature(algorithm)
        privateSignature.initSign(privateKey)
        privateSignature.update(data)
        return privateSignature.sign()
    }


    override fun encryptSecretKey(secretKey: SecretKey, publicKey: PublicKey): ByteArray {
        return doEncryptSecretKey(secretKey, publicKey)
    }

    override fun doEncryptSecretKey(secretKey: SecretKey, key: PublicKey): ByteArray {
        /*
*  https://stackoverflow.com/questions/32033804/which-padding-is-used-by-javax-crypto-cipher-for-rsa
*
*  To make it complete, if you also need integrity/authenticity, you should probably go for RSA-PSS sign using SHA-256,
*  then GCM encrypt it with a random key and IV (prefix the IV), then encrypt the GCM key using OAEP (and hope that the OAEP implementation is secure).
*  RSA-KEM is also great, but not implemented in Oracle's Java. Aint crypto fun :) – M
*
*  The default for bouncy-castle when you just specify RSA is RSA/NONE/NOPADDING
*  This is the same result as RSA/ECB/NOPADDING as well.
* */
        //var cipher = SecurityProvider.getCipher("RSA/ECB/OAEPWithSHA-256AndMGF1Padding") // TODO RSA/ECB/OAEPWithSHA-256AndMGF1Padding , RSA
        //cipher.init(Cipher.ENCRYPT_MODE, key)
        //return cipher.doFinal(symmetricKey.encoded)

        //On Android RSA/ECB/OAEPWithSHA-256AndMGF1Padding should be best defined like this
        val cipher = SecurityProvider.getCipher("RSA/ECB/OAEPPadding")
                .apply {
                    // To use SHA-256 the main digest and SHA-1 as the MGF1 digest
                    //init(Cipher.ENCRYPT_MODE, key, OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT))
                    // To use SHA-256 for both digests
                    init(Cipher.ENCRYPT_MODE,
                            key,
                            OAEPParameterSpec("SHA-256",
                                    "MGF1",
                                    MGF1ParameterSpec.SHA256,
                                    PSource.PSpecified.DEFAULT))
                }

        return cipher.doFinal(secretKey.encoded)
    }

    override fun decryptSecretKey(secretKeyEncrypted: ByteArray): SecretKey {
        return doDecryptSecretKey(secretKeyEncrypted)

    }

    override fun doDecryptSecretKey(secretKeyEncrypted: ByteArray, key: PrivateKey): SecretKey {
        //val cipher = SecurityProvider.getCipher("RSA/ECB/OAEPWithSHA-256AndMGF1Padding")// RSA
        //cipher.init(Cipher.DECRYPT_MODE, key)
        //var decrypted = cipher.doFinal(toDecrypt)

        //On Android RSA/ECB/OAEPWithSHA-256AndMGF1Padding should be best defined like this
        val cipher = SecurityProvider.getCipher("RSA/ECB/OAEPPadding")
                .apply {
                    // To use SHA-256 the main digest and SHA-1 as the MGF1 digest
                    //init(Cipher.DECRYPT_MODE, key, OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT))
                    // To use SHA-256 for both digests TODO check on Android Keystore
                    init(Cipher.DECRYPT_MODE,
                            key,
                            OAEPParameterSpec("SHA-256",
                                    "MGF1",
                                    MGF1ParameterSpec.SHA256,
                                    PSource.PSpecified.DEFAULT))
                }

        val decrypted = cipher.doFinal(secretKeyEncrypted)
        // TODO Android PKCS5Padding?
        return SecretKeySpec(decrypted, 0, decrypted.size, "AES/CBC/PKCS7Padding")
    }

    override fun checkCryptId(publicKey: ByteArray, signature: ByteArray): Boolean {
        TODO("Not yet implemented")
    }

    override fun checkCryptId(publicKey: PublicKey, signature: ByteArray): Boolean {
        TODO("Not yet implemented")
    }
}