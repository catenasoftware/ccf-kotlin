package software.catena.ccf.crypto

import java.security.SecureRandom

object KeyPairFactory {

    @JvmStatic
    fun generateKeyPairRsa(): AbstractKeyPair {
        val keySize = 4096
        val generator = SecurityProvider.getKeyPairGenerator("RSA")
        generator.initialize(keySize, SecureRandom())
        val keyPair = generator.generateKeyPair()
        return RsaKeyPair(keyPair, RsaCryptoModule(keyPair).generateCryptId())
    }

}