package software.catena.ccf.crypto

import java.security.KeyPair
import java.security.PrivateKey
import java.security.PublicKey

abstract class AbstractKeyPair(val keyPair: KeyPair, val cryptId: CryptId) {
    val publicKey: PublicKey get() = keyPair.public
    val privateKey: PrivateKey get() =  keyPair.private
    val publicKeyEncoded: ByteArray get() = publicKey.encoded
}