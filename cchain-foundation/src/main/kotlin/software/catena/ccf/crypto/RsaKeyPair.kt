package software.catena.ccf.crypto

import java.security.KeyPair

class RsaKeyPair(keyPair: KeyPair, cryptId: CryptId)
    :AbstractKeyPair(keyPair = keyPair, cryptId = cryptId)
