package software.catena.ccf.crypto

import software.catena.ccf.utils.FoundationException
import java.security.KeyStore
import java.security.cert.CertificateFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory

object SelfSigned {

    private val ccmEccCert = KeyHandling.getCertificateFromPem("""
    -----BEGIN CERTIFICATE-----
    MIIBHzCBxqADAgECAgYBbfngz9YwCgYIKoZIzj0EAwIwDjEMMAoGA1UEAwwDQ0NN
    MB4XDTE5MTAyMzE4Mjg0M1oXDTIwMTAyMzE4Mjg0M1owDjEMMAoGA1UEAwwDQ0NN
    MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEZVLY8MXa3Me0Hg0U0sGyhO9/dHdd
    f+oJRi7YuNzEN6qEYVm7mO8OrKCKmhIZsrN1uTsdebLP5m/aHyBAfrii9qMQMA4w
    DAYDVR0TAQH/BAIwADAKBggqhkjOPQQDAgNIADBFAiAIB/AyZNRJIPmuLCTqnTzz
    VZOWlG0bZGRnIxGtdySTngIhANeQWah7qxwIaC2vKQBCD06J4V2MySTMLT2mGCuc
    5T8H
    -----END CERTIFICATE-----
    """.trimIndent())

    var sslContext: SSLContext? = null
    var trustManagers: Array<TrustManager>? = null

    init {
        try {
            val keyStore =
                KeyStore.getInstance(KeyStore.getDefaultType())
            keyStore.load(null, null)

            val certificateFactory =
                CertificateFactory.getInstance("X.509")

            keyStore.setCertificateEntry("ccm", ccmEccCert)

            val trustManagerFactory =
                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(keyStore)
            trustManagers = trustManagerFactory.trustManagers
            sslContext = SSLContext.getInstance("TLS")
            sslContext!!.init(null, trustManagers, null)
            sslContext
        } catch (e: Exception) {
            throw FoundationException("Error loading server certificate")
        }
    }
}