package software.catena.ccf.crypto

import org.bouncycastle.util.encoders.Hex
import java.nio.charset.StandardCharsets
import java.security.*
import java.security.spec.MGF1ParameterSpec
import java.util.*
import javax.crypto.*
import javax.crypto.spec.OAEPParameterSpec
import javax.crypto.spec.PSource
import javax.crypto.spec.SecretKeySpec


object RSA {

    @JvmOverloads
    @Throws(NoSuchAlgorithmException::class, NoSuchProviderException::class)
    fun generateKeyPair(keySize: Int = 2048): KeyPair {//TODO key size 4096
        val generator = SecurityProvider.getKeyPairGenerator("RSA")
        generator.initialize(keySize, SecureRandom())
        return generator.generateKeyPair()
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validatePlainBytes(
        plain: ByteArray,
        signature_ASN1_DER_base64: String?,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            plain,
            Base64.getDecoder().decode(signature_ASN1_DER_base64),
            "SHA256withRSA/PSS", //TODO SHA512withRSA/PSS  SHA256withRSA
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validatePlainBytes(
        plain: ByteArray,
        signature_ASN1_DER: ByteArray,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            plain,
            signature_ASN1_DER,
            "SHA256withRSA/PSS", // SHA512withRSA/PSS, SHA256withRSA
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validatePlainText(
        plainText: String,
        signature_ASN1_DER_base64: String?,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            plainText.toByteArray(StandardCharsets.UTF_8),
            Base64.getDecoder().decode(signature_ASN1_DER_base64),
            "SHA256withRSA/PSS", // SHA512withRSA/PSS, SHA256withRSA
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validatePlainText(
        plainText: String,
        signature_ASN1_DER: ByteArray,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            plainText.toByteArray(StandardCharsets.UTF_8),
            signature_ASN1_DER,
            "SHA256withRSA/PSS", // SHA512withRSA/PSS, SHA256withRSA
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validateHash(
        sha256hex: String?,
        signature_ASN1_DER_base64: String?,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            Hex.decode(sha256hex),
            Base64.getDecoder().decode(signature_ASN1_DER_base64),
            "NONEwithRSA/PSS", // NONEwithRSA
            publicKey
        )
    }

    @Throws(
        NoSuchProviderException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class
    )
    fun validate(
        data: ByteArray,
        signature_ASN1_DER: ByteArray,
        algorithm: String,
        publicKey: PublicKey,
    ): Boolean {

        val publicSignature = SecurityProvider.getSignature(algorithm)
        publicSignature.initVerify(publicKey)
        publicSignature.update(data)
        return publicSignature.verify(signature_ASN1_DER)
    }

    @Throws(
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class,
        NoSuchProviderException::class
    )
    fun signPlainBytes(plain: ByteArray, privateKey: PrivateKey): ByteArray {
        return sign(plain, "SHA256withRSA/PSS", privateKey) // SHA512withRSA/PSS, SHA256withRSA
    }

//    @Throws(
//        NoSuchAlgorithmException::class,
//        InvalidKeyException::class,
//        SignatureException::class,
//        NoSuchProviderException::class
//    )
//    fun signPlainText(plainText: String, privateKey: PrivateKey): ByteArray {
//        return sign(
//            plainText.toByteArray(StandardCharsets.UTF_8),
//            "SHA256withRSA/PSS", // SHA512withRSA/PSS, SHA256withRSA
//            privateKey
//        )
//    }

    @Throws(
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class,
        NoSuchProviderException::class
    )
    fun signHash(sha256hex: String?, privateKey: PrivateKey): ByteArray {
        return sign(
            Hex.decode(sha256hex),
            "NONEwithRSA/PSS", // "NONEwithRSA"
            privateKey
        )
    }

    @Throws(
        NoSuchProviderException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class
    )
    fun sign(data: ByteArray, algorithm: String, privateKey: PrivateKey): ByteArray {
        val privateSignature = SecurityProvider.getSignature(algorithm)
        privateSignature.initSign(privateKey)
        privateSignature.update(data)
        return privateSignature.sign()
    }

    @Throws(
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class,
        InvalidKeyException::class,
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class
    )
    fun encryptSymmetricKey(symmetricKey: SecretKey, key: PublicKey): ByteArray {
        /*
        *  https://stackoverflow.com/questions/32033804/which-padding-is-used-by-javax-crypto-cipher-for-rsa
        *
        *  To make it complete, if you also need integrity/authenticity, you should probably go for RSA-PSS sign using SHA-256,
        *  then GCM encrypt it with a random key and IV (prefix the IV), then encrypt the GCM key using OAEP (and hope that the OAEP implementation is secure).
        *  RSA-KEM is also great, but not implemented in Oracle's Java. Aint crypto fun :) – M
        *
        *  The default for bouncy-castle when you just specify RSA is RSA/NONE/NOPADDING
        *  This is the same result as RSA/ECB/NOPADDING as well.
        * */
        //var cipher = SecurityProvider.getCipher("RSA/ECB/OAEPWithSHA-256AndMGF1Padding") // TODO RSA/ECB/OAEPWithSHA-256AndMGF1Padding , RSA
        //cipher.init(Cipher.ENCRYPT_MODE, key)
        //return cipher.doFinal(symmetricKey.encoded)

        //On Android RSA/ECB/OAEPWithSHA-256AndMGF1Padding should be best defined like this
        val cipher = SecurityProvider.getCipher("RSA/ECB/OAEPPadding")
            .apply {
                // To use SHA-256 the main digest and SHA-1 as the MGF1 digest
                //init(Cipher.ENCRYPT_MODE, key, OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT))
                // To use SHA-256 for both digests
                init(Cipher.ENCRYPT_MODE,
                    key,
                    OAEPParameterSpec("SHA-256",
                        "MGF1",
                        MGF1ParameterSpec.SHA256,
                        PSource.PSpecified.DEFAULT))
            }

        return cipher.doFinal(symmetricKey.encoded)

    }

    @Throws(
        InvalidKeyException::class,
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class,
        InvalidAlgorithmParameterException::class
    )
    fun decryptSymmetricKey(toDecrypt: ByteArray, key: PrivateKey): SecretKey {
        //val cipher = SecurityProvider.getCipher("RSA/ECB/OAEPWithSHA-256AndMGF1Padding")// RSA
        //cipher.init(Cipher.DECRYPT_MODE, key)
        //var decrypted = cipher.doFinal(toDecrypt)

        //On Android RSA/ECB/OAEPWithSHA-256AndMGF1Padding should be best defined like this
        val cipher = SecurityProvider.getCipher("RSA/ECB/OAEPPadding")
            .apply {
                // To use SHA-256 the main digest and SHA-1 as the MGF1 digest
                //init(Cipher.DECRYPT_MODE, key, OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT))
                // To use SHA-256 for both digests TODO check on Android Keystore
                init(Cipher.DECRYPT_MODE,
                    key,
                    OAEPParameterSpec("SHA-256",
                        "MGF1",
                        MGF1ParameterSpec.SHA256,
                        PSource.PSpecified.DEFAULT))
            }

        var decrypted = cipher.doFinal(toDecrypt)

        val symmetricKey: SecretKey =
            //TODO PKCS7?
            SecretKeySpec(
                decrypted,
                0,
                decrypted.size,
                "AES/CBC/PKCS7Padding"
            ) // TODO Android PKCS5Padding?

        return symmetricKey
    }

}