package software.catena.ccf.crypto

import org.bouncycastle.util.encoders.Hex
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

object Hash {

    fun getSHA256Hex(inputUTF8: String): String {
        val hash = getSHA256Bytes(inputUTF8)
        return String(Hex.encode(hash), Charsets.UTF_8)
    }

    @Throws(
        NoSuchAlgorithmException::class
    )
    fun getSHA256Bytes(inputUTF8: String): ByteArray {
        var digest: MessageDigest? = MessageDigest.getInstance("SHA-256")

        return digest!!.digest(inputUTF8.toByteArray(StandardCharsets.UTF_8))
    }

    fun getSHA256Hex(input: ByteArray): String {
        val hash = getSHA256Bytes(input)
        return String(Hex.encode(hash), Charsets.UTF_8)
    }

    @Throws(
        NoSuchAlgorithmException::class
    )
    fun getSHA256Bytes(input: ByteArray): ByteArray {
        var digest: MessageDigest? = MessageDigest.getInstance("SHA-256")

        return digest!!.digest(input)
    }
}