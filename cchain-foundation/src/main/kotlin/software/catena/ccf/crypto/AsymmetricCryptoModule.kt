package software.catena.ccf.crypto

import software.catena.ccf.models.Pair
import software.catena.ccf.models.Reflection
import software.catena.ccf.models.transaction.TransactionRequestDataToHash
import java.security.KeyPair
import java.security.PrivateKey
import java.security.PublicKey
import java.util.*
import java.util.function.Consumer
import javax.crypto.SecretKey

abstract class AsymmetricCryptoModule(val keyPair: KeyPair) {

    fun generateCryptId(): CryptId {
        return CryptId(keyPair.public, signPlainBytes(keyPair.public.encoded))
    }

    private fun getMembersValueAsConcatenatedString(o: Any): String {
        val pairList = Reflection.getMembers(o)
        val stringList = LinkedList<String>()
        pairList.forEach(Consumer { item: Pair ->
            stringList.add(item.value)
        })
        return java.lang.String.join(" \\\\ ", stringList)
    }

    fun signObjectMemberValues(o: Any): ByteArray {
        return signPlainText(getMembersValueAsConcatenatedString(o))
    }

    abstract fun getCryptoAlgorithm(): TransactionRequestDataToHash.Algorithm

    abstract fun validatePlainBytes(plain: ByteArray, signature: ByteArray): Boolean

    abstract fun validatePlainText(plainText: String, signature: ByteArray): Boolean


    protected abstract fun validate(data: ByteArray,
                                    signature_ASN1_DER: ByteArray,
                                    algorithm: String,
                                    publicKey: PublicKey = keyPair.public): Boolean


    abstract fun signPlainText(plainText: String): ByteArray

    abstract fun signPlainBytes(plain: ByteArray): ByteArray

    protected abstract fun sign(data: ByteArray, algorithm: String, privateKey: PrivateKey = keyPair.private): ByteArray

    abstract fun encryptSecretKey(secretKey: SecretKey, publicKey: PublicKey = keyPair.public): ByteArray

    protected abstract fun doEncryptSecretKey(secretKey: SecretKey, key: PublicKey = keyPair.public): ByteArray

    abstract fun decryptSecretKey(secretKeyEncrypted: ByteArray): SecretKey

    protected abstract fun doDecryptSecretKey(secretKeyEncrypted: ByteArray, key: PrivateKey = keyPair.private): SecretKey

    abstract fun checkCryptId(publicKey: ByteArray, signature: ByteArray): Boolean

    abstract fun checkCryptId(publicKey: PublicKey, signature: ByteArray): Boolean

}