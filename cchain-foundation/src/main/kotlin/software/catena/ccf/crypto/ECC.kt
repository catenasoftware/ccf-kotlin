package software.catena.ccf.crypto

import org.bouncycastle.util.encoders.Hex
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import java.security.*
import java.security.spec.ECGenParameterSpec
import java.util.*

object ECC {

    @Throws(
        InvalidAlgorithmParameterException::class,
        NoSuchAlgorithmException::class,
        NoSuchProviderException::class
    )
    fun generateKeyPair(): KeyPair {
        return generateKeyPair(ECGenParameterSpec("secp256r1"))
    }

    @Throws(
        NoSuchProviderException::class,
        NoSuchAlgorithmException::class,
        InvalidAlgorithmParameterException::class
    )
    fun generateKeyPair(parameterSpec: ECGenParameterSpec?): KeyPair {
        val keygen = SecurityProvider.getKeyPairGenerator("EC")
        keygen.initialize(parameterSpec, SecureRandom())
        return keygen.generateKeyPair()
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validatePlainBytes(
        plain: ByteArray,
        signature_ASN1_DER_base64: String?,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            plain,
            Base64.getDecoder().decode(signature_ASN1_DER_base64),
            "SHA256withECDSA",
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validatePlainBytes(
        plain: ByteArray,
        signature_ASN1_DER: ByteArray,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            plain,
            signature_ASN1_DER,
            "SHA256withECDSA",
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validatePlainText(
        plainText: String,
        signature_ASN1_DER_base64: String?,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            plainText.toByteArray(StandardCharsets.UTF_8),
            Base64.getDecoder().decode(signature_ASN1_DER_base64),
            "SHA256withECDSA",
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validatePlainText(
        plainText: String,
        signature_ASN1_DER: ByteArray,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            plainText.toByteArray(StandardCharsets.UTF_8),
            signature_ASN1_DER,
            "SHA256withECDSA",
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validateHash(
        sha256hex: String?,
        signature_ASN1_DER_base64: String?,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            Hex.decode(sha256hex),
            Base64.getDecoder().decode(signature_ASN1_DER_base64),
            "NONEwithECDSA",
            publicKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        SignatureException::class,
        InvalidKeyException::class,
        NoSuchProviderException::class
    )
    fun validateHash(
        sha256hex: String?,
        signature_ASN1_DER: ByteArray,
        publicKey: PublicKey,
    ): Boolean {
        return validate(
            Hex.decode(sha256hex),
            signature_ASN1_DER,
            "NONEwithECDSA",
            publicKey
        )
    }

    @Throws(
        NoSuchProviderException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class
    )
    fun validate(
        data: ByteArray,
        signature_ASN1_DER: ByteArray,
        algorithm: String,
        publicKey: PublicKey,
    ): Boolean {
        val publicSignature = SecurityProvider.getSignature(algorithm)
        publicSignature.initVerify(publicKey)
        publicSignature.update(data)
        return publicSignature.verify(signature_ASN1_DER)
    }

    @Throws(
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class,
        NoSuchProviderException::class
    )
    fun signPlainBytes(plain: ByteArray, privateKey: PrivateKey): ByteArray {
        return sign(plain, "SHA256withECDSA", privateKey)
    }

    @Throws(
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class,
        NoSuchProviderException::class
    )
    fun signPlainText(plainText: String, privateKey: PrivateKey): ByteArray {
        return sign(
            plainText.toByteArray(StandardCharsets.UTF_8),
            "SHA256withECDSA",
            privateKey
        )
    }

    @Throws(
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class,
        NoSuchProviderException::class
    )
    fun signHash(sha256hex: String?, privateKey: PrivateKey): ByteArray {
        return sign(
            Hex.decode(sha256hex),
            "NONEwithECDSA",
            privateKey
        )
    }

    @Throws(
        NoSuchProviderException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class
    )
    fun sign(data: ByteArray, algorithm: String, privateKey: PrivateKey): ByteArray {
        val privateSignature = SecurityProvider.getSignature(algorithm)
        privateSignature.initSign(privateKey)
        privateSignature.update(data)
        return privateSignature.sign()
    }

    @Throws(
        InvalidKeyException::class,
        NoSuchProviderException::class,
        NoSuchAlgorithmException::class,
        InvalidAlgorithmParameterException::class,
        IOException::class
    )
    /*fun doECDH(
        recipientPublicKey: ECPublicKey?,
        senderPrivateKey: ECPrivateKey?,
        transactionId: UUID
    ): ByteArray {
        Security.addProvider(BouncyCastleProvider())
        val ka = KeyAgreement.getInstance("ECDH", BouncyCastleProvider.PROVIDER_NAME)
        ka.init(senderPrivateKey)
        ka.doPhase(recipientPublicKey, true)
        val agreedKey = ka.generateSecret()
        return getDerivedSecret(agreedKey, transactionId)
    }*/

    private fun getBytesFromUUID(uuid: UUID): ByteArray {
        val bb = ByteBuffer.wrap(ByteArray(16))
        bb.putLong(uuid.mostSignificantBits)
        bb.putLong(uuid.leastSignificantBits)
        return bb.array()
    }

    /*@Throws(IOException::class)
    private fun getDerivedSecret(dhKey: ByteArray, transactionId: UUID): ByteArray {
        val transactionIdBytes = getBytesFromUUID(transactionId)
        val builder = DEROtherInfo.Builder(
            AlgorithmIdentifier(NISTObjectIdentifiers.id_aes128_CCM.intern()),
            transactionIdBytes,
            transactionIdBytes
        )
        val otherInfo = builder.build()
        val param = KDFParameters(dhKey, otherInfo.encoded)
        val kdfGenerator =
            ConcatenationKDFGenerator(DigestFactory.createSHA256())
        kdfGenerator.init(param)
        val output = ByteArray(16)
        kdfGenerator.generateBytes(output, 0, 16)
        return output
    }*/
}