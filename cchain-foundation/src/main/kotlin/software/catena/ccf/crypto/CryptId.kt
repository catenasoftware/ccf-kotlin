package software.catena.ccf.crypto

import java.security.PublicKey

data class CryptId(val publicKey: PublicKey, val publicKeySigned: ByteArray) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CryptId

        if (publicKey != other.publicKey) return false
        if (!publicKeySigned.contentEquals(other.publicKeySigned)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = publicKey.hashCode()
        result = 31 * result + publicKeySigned.contentHashCode()
        return result
    }
}