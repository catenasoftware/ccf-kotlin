package software.catena.ccf.crypto

import software.catena.ccf.utils.FoundationException
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import org.bouncycastle.asn1.ASN1Sequence
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers
import org.bouncycastle.openssl.jcajce.JcaPEMWriter
import org.bouncycastle.util.io.pem.PemObject
import org.bouncycastle.util.io.pem.PemReader
import java.io.*
import java.security.*
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.security.spec.InvalidKeySpecException
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*

object KeyHandling {

    @Throws(Exception::class)
    // algrithms: RSA, ECDSA
    fun getPrivateKeyFromPemFile(filename: String, algorithm: String = "ECDSA"): PrivateKey {
        val pemObject = getPemFile(filename)
        val content = pemObject!!.content
        return generatePKCS8(content, algorithm)
    }

    @Throws(Exception::class)
    // algrithms: RSA, ECDSA
    fun getPublicKeyFromPemFile(filename: String, algorithm: String = "ECDSA"): PublicKey {
        val pemObject = getPemFile(filename)
        val content = pemObject!!.content
        return generateX509(content, algorithm)
    }

    @Throws(Exception::class)
    fun savePrivateKeyAsPemFile(privateKey: PrivateKey, filename: String?): String {
        val fileWriter = FileWriter(filename)
        val jcaPEMWriter = JcaPEMWriter(fileWriter)
        val pemObject = PemObject("PRIVATE KEY", privateKey.encoded)
        jcaPEMWriter.writeObject(pemObject)
        jcaPEMWriter.flush()
        jcaPEMWriter.close()
        return Base64.getEncoder().encodeToString(pemObject.content)
    }

    @Throws(CertificateException::class, FileNotFoundException::class)
    fun getCertificateFromPemFile(filename: String): X509Certificate {
        val cf =
            CertificateFactory.getInstance("X.509")
        return cf.generateCertificate(FileInputStream(filename)) as X509Certificate
    }

    @Throws(CertificateException::class)
    fun getCertificateFromPem(pemContent: String): X509Certificate {
        return getCertificateFromPem(
            pemContent.toByteArray(Charsets.UTF_8)
        )
    }

    @Throws(CertificateException::class)
    fun getCertificateFromPemBase64(pemContent64: String): X509Certificate {
        return getCertificateFromPem(
            Base64.getDecoder().decode(pemContent64)
        )
    }

    @Throws(CertificateException::class)
    fun getCertificateFromPem(pemContent: ByteArray): X509Certificate {
        val cf =
            CertificateFactory.getInstance("X.509")
        val content = ByteArrayInputStream(pemContent)
        return cf.generateCertificate(content) as X509Certificate
    }

    @Throws(Exception::class)
    fun savePublicKeyAsPemFile(publicKey: PublicKey, filename: String?): String {
        val fileWriter = FileWriter(filename)
        val jcaPEMWriter = JcaPEMWriter(fileWriter)
        val pemObject = PemObject("PUBLIC KEY", publicKey.encoded)
        jcaPEMWriter.writeObject(pemObject)
        jcaPEMWriter.flush()
        jcaPEMWriter.close()
        return Base64.getEncoder().encodeToString(pemObject.content)
    }

    @Throws(NoSuchAlgorithmException::class, InvalidKeySpecException::class)
    fun getPublicKeyFromPem(pemContent64: String?, algorithm: String): PublicKey {
        val content = Base64.getDecoder().decode(pemContent64)
        return generateX509(content, algorithm)
    }

    @Throws(IOException::class)
    fun getPublicKeyFromASN(pemContent: ByteArray?): PublicKey {
        val spki = SubjectPublicKeyInfo.getInstance(pemContent)
        return SecurityProvider.getPublicKeyFromASN(spki)
    }

    @Throws(NoSuchAlgorithmException::class, InvalidKeySpecException::class)
    fun getPrivateKeyFromPem(pemContent64: String?, algorithm: String): PrivateKey {
        val content = Base64.getDecoder().decode(pemContent64)
        return generatePKCS8(content, algorithm)
    }

    @Throws(NoSuchAlgorithmException::class, InvalidKeySpecException::class)
    private fun generatePKCS8(content: ByteArray, algorithm: String): PrivateKey {
        var alg = algorithm
        val spec = PKCS8EncodedKeySpec(content)
        if (alg == "ECDSA") {
            alg = "EC"
        }
        val kf = KeyFactory.getInstance(alg)
        return kf.generatePrivate(spec)
    }

    @Throws(InvalidKeySpecException::class, NoSuchAlgorithmException::class)
    private fun generateX509(content: ByteArray, algorithm: String): PublicKey {
        var alg = algorithm
        val spec = X509EncodedKeySpec(content)
        if (alg == "ECDSA") {
            alg = "EC"
        }
        val kf = KeyFactory.getInstance(alg)
        return kf.generatePublic(spec)
    }

    @Throws(IOException::class)
    private fun getPemFile(filename: String): PemObject? {
        var pemObject: PemObject? = null
        PemReader(InputStreamReader(FileInputStream(filename))).use { pemReader ->
            pemObject = pemReader.readPemObject()
        }
        return pemObject
    }

    @Throws(
        IOException::class,
        NoSuchProviderException::class,
        NoSuchAlgorithmException::class,
        InvalidKeyException::class,
        SignatureException::class
    )
    fun checkCryptId(publicKeyASN1: ByteArray, signature: ByteArray): Boolean {
        val pk = getPublicKeyFromASN(publicKeyASN1)
        return when (pk.algorithm) {
            "RSA" -> RSA.validatePlainBytes(publicKeyASN1, signature, pk)
            "EC" -> ECC.validatePlainBytes(publicKeyASN1, signature, pk)
            else -> throw FoundationException(
                "PublicKey's algorithm is not RSA or EC and is not implemented yet!"
            )
        }
    }

    @Throws(Exception::class)
    fun getOID(publicKeyASN1: ByteArray?): ASN1ObjectIdentifier {
        val spki =
            SubjectPublicKeyInfo.getInstance(ASN1Sequence.getInstance(publicKeyASN1))
        val algid = spki.algorithm
        return when (algid.algorithm) {
            PKCSObjectIdentifiers.rsaEncryption -> algid.algorithm as ASN1ObjectIdentifier
            X9ObjectIdentifiers.id_ecPublicKey -> algid.parameters as ASN1ObjectIdentifier
            else -> throw FoundationException(
                "OID can't be determined. PublicKey is not RSA or EC and is not implemented yet!"
            )
        }
    }
}