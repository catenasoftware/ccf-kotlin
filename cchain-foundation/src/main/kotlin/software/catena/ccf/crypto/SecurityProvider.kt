package software.catena.ccf.crypto

import software.catena.ccf.utils.FoundationException
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.security.KeyPairGenerator
import java.security.PublicKey
import java.security.Security
import java.security.Signature
import javax.crypto.Cipher

object SecurityProvider {

    // This explicit check avoids activating in Android Studio with Android specific classes
    // available when running plugins inside the IDE.
    private val isAndroid: Boolean = "Dalvik" == System.getProperty("java.vm.name")

    init {
        if (isAndroid) {
            if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) != null)
                BouncyCastleProvider()
        } else {
            if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null)
                Security.addProvider(BouncyCastleProvider())
        }
    }

    fun getKeyPairGenerator(algorithm: String): KeyPairGenerator {
        return if (isAndroid)
            throw FoundationException("Please use Android Keystore for key generation.")
        else
            KeyPairGenerator.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME)
    }

    fun getCipher(algorithm: String): Cipher {
        return if (isAndroid)
            Cipher.getInstance(algorithm)
        else
            Cipher.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME)
    }

    fun getSignature(algorithm: String): Signature {
        return if (isAndroid)
            Signature.getInstance(algorithm)
        else
            Signature.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME)
    }

    fun getPublicKeyFromASN(spki: SubjectPublicKeyInfo): PublicKey {
        return BouncyCastleProvider.getPublicKey(spki)
    }


}