package software.catena.ccf.crypto

import org.bouncycastle.util.encoders.Hex

object Hex : Hex() {

    fun toHexStringNullable(var0: ByteArray?): String? {
        return if (var0 == null) {
            null
        } else toHexString(var0, 0, var0.size)
    }
}