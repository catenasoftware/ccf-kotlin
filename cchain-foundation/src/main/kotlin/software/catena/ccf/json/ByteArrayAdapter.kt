package software.catena.ccf.json

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.*

class ByteArrayAdapter {
    @ToJson
    fun toJson(value: ByteArray): String {
        return Base64.getEncoder().encodeToString(value)
    }

    @FromJson
    fun fromJson(value: String): ByteArray {
        return Base64.getDecoder().decode(value)
    }
}