package software.catena.ccf.json

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.OffsetDateTime

class OffsetDateTimeAdapter {
    @ToJson
    fun toJson(value: OffsetDateTime): String {
        return value.toString()
    }

    @FromJson
    fun fromJson(value: String): OffsetDateTime? {
        return OffsetDateTime.parse(value)
    }

}