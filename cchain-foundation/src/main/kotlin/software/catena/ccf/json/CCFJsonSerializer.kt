package software.catena.ccf.json

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okio.BufferedSource
import org.bouncycastle.util.encoders.UTF8
import java.util.*

object CCFJsonSerializer {

    @JvmStatic
    // TODO PW: make moshi private
    val moshi: Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
        .add(OffsetDateTimeAdapter())
        .add(UUIDAdapter())
        .add(ByteArrayAdapter())
        .add(ArrayListAdapter.FACTORY)
        .build()


    fun toJson(o: Any): String {
        return moshi.adapter(o.javaClass).toJson(o)
    }

    fun toJsonBytesUtf8(o: Any): ByteArray {
        return moshi.adapter(o.javaClass).toJson(o).toByteArray(Charsets.UTF_8)
    }

    fun <T> fromJson(clazz: Class<T>, jsonString: String): T?{
        return moshi.adapter(clazz).fromJson(jsonString)
    }

    fun <T> fromJson(clazz: Class<T>, bufferedSource: BufferedSource?): T?{
        return moshi.adapter(clazz).fromJson(bufferedSource)
    }

    fun <T> fromJson(clazz: Class<T>, bytes: ByteArray): T?{
        return fromJson(clazz, String(bytes, Charsets.UTF_8))
    }
}

